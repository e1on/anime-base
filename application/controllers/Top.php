<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Top extends CI_Controller {

	private function  genTop ($sorting = '') {

		$this->load->library('paginator');
		$this->paginator->initialize(8, 'page');
		$this->paginator->set_total($this->db->query("SELECT * FROM `anime`")->num_rows());

		$this->load->model ("TopModel");
		return $this->TopModel->getTop ($sorting, $this->paginator->get_limit());
	}

	public function index() {

		$this->load->library('paginator');
		$animes = $this->genTop ();

		$this->load->view('header', ['title' => 'Рейтинг аниме - Аниме база'.($this->paginator->_page > 1 ? ' > Страница '.$this->paginator->_page : ''), 'description' => 'Общий аниме ТОП, лучшие аниме смотреть онлайн в высоком качестве'.($this->paginator->_page > 1 ? ' > Страница '.$this->paginator->_page : '')]);
		$this->load->view('top/main', ['anime' => $animes, 'page_links' => $this->paginator->page_links (), 'action' => 'index']);
		$this->load->view('footer');

	}

	public function today () {

		$this->load->library('paginator');
		$animes = $this->genTop ('today');

		$this->load->view('header', ['title' => 'Рейтинг аниме за этот день - Аниме база'.($this->paginator->_page > 1 ? ' > Страница '.$this->paginator->_page : ''), 'description' => 'Аниме ТОП за сутки, лучшие аниме смотреть онлайн в высоком качестве'.($this->paginator->_page > 1 ? ' > Страница '.$this->paginator->_page : '')]);
		$this->load->view('top/main', ['anime' => $animes, 'page_links' => $this->paginator->page_links (), 'action' => 'today']);
		$this->load->view('footer');

	}

	public function week () {

		$this->load->library('paginator');
		$animes = $this->genTop ('week');

		$this->load->view('header', ['title' => 'Рейтинг аниме за неделю - Аниме база'.($this->paginator->_page > 1 ? ' > Страница '.$this->paginator->_page : ''), 'description' => 'Аниме ТОП за неделю, лучшие аниме смотреть онлайн в высоком качестве'.($this->paginator->_page > 1 ? ' > Страница '.$this->paginator->_page : '')]);
		$this->load->view('top/main', ['anime' => $animes, 'page_links' => $this->paginator->page_links (),'action' => 'week']);
		$this->load->view('footer');

	}

}
