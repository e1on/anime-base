<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Genres extends CI_Controller {

	public function index () {

		$data = [];

		$this->load->model ("GenreModel");
		$data['genres'] = $this->GenreModel->getAllhandler ();

		$this->load->view('header', ['title' => 'Аниме по жанрам', 'description' => 'Смотреть аниме по жанрам онлайн в хорошем качестве']);
		$this->load->view('genres/main', $data);
		$this->load->view('footer');

	}

	public function view($id = 'id', $valueId = 0, $addToList='', $valueAddToList = '') {

		$data = [];

		$this->load->model ("GenreModel");
		if ($this->GenreModel->check ($valueId) < 1) {
			header("Location: /Genres");
			exit;
		}

		$data['genre'] = $this->GenreModel->getById ($valueId);

		$this->load->library('paginator');
		$this->paginator->initialize(8, 'page');
		$this->paginator->set_total($this->db->query("SELECT * FROM `anime` WHERE `genres` LIKE '%:".$valueId.":%'")->num_rows());

		$this->load->model ("AnimeModel");
		$data['anime'] = $this->AnimeModel->getAllByGanre ($valueId, "ORDER BY `views` DESC ".$this->paginator->get_limit());
		$data['page_links'] = $this->paginator->page_links ();

		$this->load->view('header', ['title' => 'Аниме '.$data['genre']->name.' онлайн '.($this->paginator->_page > 1 ? ' > Страница '.$this->paginator->_page : ''), 'description' => 'Аниме '.$data['genre']->name.', смотреть онлайн в высоком качестве '.($this->paginator->_page > 1 ? ' > Страница '.$this->paginator->_page : '')]);
		$this->load->view('genres/view', $data);
		$this->load->view('footer');

	}

}