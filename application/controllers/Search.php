<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CI_Controller {

	public function index () {

		$this->load->model ("SearchModel");
		$data = [];

		$this->load->model ("GenreModel");
		$data['genres'] = $this->GenreModel->getAll ();

		$this->load->model ("VoiceModel");
		$data['voices'] = $this->VoiceModel->getAll ();

		$this->load->library('paginator');

		if (isset($_GET['name'])) {

			if (isset($_GET['genres'][0])) {
				
				$this->load->model ("GenreModel");

				$genresSql = '';
				foreach ($_GET['genres'] as $key => $value) {
					$genresSql .= "AND `genres` LIKE '%:".intval (abs ($value)).":%'";

					$genreData = $this->GenreModel->getById ($value);
					if ($genreData->name) {
						$data['titleGenre'][] = 'аниме '.$genreData->name;
					}

				}

				$genreData = $this->GenreModel->getById ($_GET['genres'][0]);
				if ($genreData->name) {
					$title = 'аниме '.$genreData->name;
					$description = 'аниме '.$genreData->name;
				}

			} else {

				$genresSql = '';
				$data['titleGenre'] = '';
			}

			if (isset($_GET['voices'][0])) {

				$this->load->model ("VoiceModel");

				$voicesSql = '';
				foreach ($_GET['voices'] as $key => $value) {
					$voicesSql .= "AND `voices` LIKE '%:".intval (abs ($value)).":%'";

					$voiceData = $this->VoiceModel->getById ($value);
					if ($voiceData->name) {
						$data['titleVoice'][] = 'Смотреть аниме '.$voiceData->name;
					}

				}

				$voiceData = $this->VoiceModel->getById ($_GET['voices'][0]);
				if ($voiceData->name) {
					$title = 'Аниме '.$voiceData->name;
					if (isset($description)) {
						$description = $description .' Аниме с озвучкой '.$voiceData->name;
					}
					else $description = 'Поиск аниме сериалов, смотреть онлай в хорошем качестве. Найти аниме сериал';
				}

			} else {

				$voicesSql = '';
				$title = 'Поиск аниме';
				$data['titleVoice'] = '';
				$description = 'Поиск аниме сериалов, смотреть онлай в хорошем качестве. Найти аниме сериал';
			}

			$sortSql = (isset($_GET['sort']) and $_GET['sort'] == 2) ? ' ORDER BY `year` DESC' : 'ORDER BY `views` DESC';

			$name = $this->input->get ('name');

			$sql = "SELECT * FROM `anime` WHERE `name` LIKE '%".$this->db->escape_like_str($name)."%' ".$genresSql." ".$voicesSql." ".$sortSql;


			$this->paginator->initialize(8, 'page');
			$this->paginator->set_total($this->db->query($sql)->num_rows());
			$data['page_links'] = $this->paginator->page_links ('/Search?'.$_SERVER['QUERY_STRING'].'&');

			$this->load->model ("SearchModel");
			$rows = $this->SearchModel->search (['sql' => $sql, 'limit' => $this->paginator->get_limit()]);

			$data['anime'] = $rows;
			$data['inputNameValue'] = $name;

		}
		else {
			$title = 'Поиск аниме';
			$description = 'Поиск аниме сериалов, смотреть онлай в хорошем качестве. Найти аниме сериал';
		}
		$this->load->view('header', ['title' => $title.' - Аниме база'.($this->paginator->_page > 1 ? ' > Страница '.$this->paginator->_page : ''), 'description' => $description.' '.($this->paginator->_page > 1 ? ' > Страница '.$this->paginator->_page : '')]);
		$this->load->view('search/main', $data);
		$this->load->view('footer');

	}

}
