<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Anime extends CI_Controller {

	public function index($urlName='', $addToList='') {

		$data = [];

		$this->load->model ("AnimeModel");
		$check = $this->AnimeModel->checkName($urlName);

		if ($check < 1) {
			header("Location: /");
			return;
		}


		$anime = $this->AnimeModel->getByName ($urlName);

		## Добавляем мнение
		if (isset($_POST['message']) and !empty($this->user->avatar)) {
			
			$message = $this->input->post ('message');

			if (!empty($message)) {

				$this->load->model ("RecallModel");
				$this->RecallModel->add ($anime->id, $this->user->id, $message);
				$data['setRecall'] = 'Добавлено :c';

			}

		}

		$this->load->model ("AnimeUserListModel");

		if (!empty($addToList)) {

			if ($this->AnimeUserListModel->checkInList($anime->id, $this->user->id) < 1) {

				$this->AnimeUserListModel->setInList($anime->id, $this->user->id);
				$data['setAnime'] = 'Анеме добавлено :c';

			}

		}

		$data['checkInList'] = $this->AnimeUserListModel->checkInList($anime->id, $this->user->id);


		// просмотр аниме
		$ip = $this->input->ip_address();
		$this->load->model ("ViewModel");
		if ($this->ViewModel->check ($anime->id, $ip) < 1) {
			$this->ViewModel->add ($anime->id, $ip);
		}

		$data['anime'] = $anime;

		$this->load->model ("AnimeSerieModel");
		$series = $this->AnimeSerieModel->getByAnimeId ($anime->id);

		$data['series'] = $series;

		$description = strip_tags($anime->description);
		$description = substr($description, 0, 400);
		$description = rtrim($description, "!,.-");
		$description = substr($description, 0, strrpos($description, ' '));

	    $pattern="#[^\d]\ [\s]*([A-ZА-Я][A-zА-я]*)#u";
	    preg_match_all($pattern, $anime->description, $match);

	    $keys = [];
	    foreach ($match[1] as $key => $value) {
	    	if (mb_strlen($value,'UTF-8') > 2 and !isset($keys[$value])) $keys[$value] = $value;
	    }

		$keys = implode(", ", $keys);

		$this->load->view('header', ['title' => 'Смотреть '.$anime->name.' онлайн', 'description' => $description.'...', 'keywords' => htmlspecialchars($keys)]);
		$this->load->view('anime/view', $data);
		$this->load->view('footer');

	}

	public function newList () {

		$data = [];

		$this->load->library('paginator');
		$this->paginator->initialize(8, 'page');
		$this->paginator->set_total($this->db->query("SELECT * FROM `anime`")->num_rows());

		$this->load->model ("AnimeModel");
		$data['anime'] = $this->AnimeModel->getNew ($this->paginator->get_limit());
		$data['page_links'] = $this->paginator->page_links ();
		
		$this->load->view('header', ['title' => 'Новые аниме - Аниме база '.($this->paginator->_page > 1 ? ' » Страница '.$this->paginator->_page : ''), 'description' => 'Свежие аниме новинки 2017 года, смотреть онлайн в хорошем качестве '.($this->paginator->_page > 1 ? ' > Страница '.$this->paginator->_page : '')]);
		$this->load->view('anime/newList', $data);
		$this->load->view('footer');

	}

	public function ongoing () {

		$data = [];
		
		$date = date("n")+2;
		if ($date == 13) $date = 1;

		if ($date == 12 or $date == 1 or $date == 2) $season = 1;
		if ($date == 3 or $date == 4 or $date == 5) $season = 2;
		if ($date == 6 or $date == 7 or $date == 8) $season = 3;
		if ($date == 9 or $date == 10 or $date == 11) $season = 4;

		$this->load->model ("OngoingSerieModel");
		$ongoingSeries = $this->OngoingSerieModel->getLimit ();
		$data['ongoingSeries'] = $ongoingSeries;

		$this->load->library('paginator');
		$this->paginator->initialize(8, 'page');
		$this->paginator->set_total($this->db->query("SELECT * FROM `anime` WHERE `year`='".date ('Y')."' AND `season` >= '".($season-1)."'")->num_rows());

		$this->load->model ("AnimeModel");
		$data['anime'] = $this->AnimeModel->getOngoings ($this->paginator->get_limit());
		$data['page_links'] = $this->paginator->page_links ();
		
		$this->load->view('header', ['title' => 'Аниме онгоинги. Новые серии аниме '.($this->paginator->_page > 1 ? ' » Страница '.$this->paginator->_page : ''), 'description' => 'Аниме онгоинги 2017 года, смотреть онлайн в высоком качестве. Аниме новинки онлайн без регистрации.  '.($this->paginator->_page > 1 ? ' > Страница '.$this->paginator->_page : '')]);
		$this->load->view('anime/ongoing', $data);
		$this->load->view('footer');

	}

}
