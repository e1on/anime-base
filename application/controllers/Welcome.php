<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index () {

		$data = [];
		$this->load->model ("AnimeModel");
		$data['anime'] = $this->AnimeModel->getOngoings ("LIMIT 8");

		$this->load->view('header', 

			['title' => 
				'Аниме база. Смотреть аниме сериалы',
			'description' => 
				'Мы собрали для вас большую аниме базу сериалов разного жанра и режисеров. Добавление новых серий каждый день, смотри первым']);

		$this->load->view('welcome', $data);

		$this->load->view('footer');

	}

}