<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vk extends CI_Controller {

    public $clientId = '5778907';
    public $securityKey = '3i7dQSUFQht5g6VIExlq';
    public $scope = 'offline';

	public function signin () {

		if (!empty($this->user->vkId)) {

			header ("Location: /");
			return;

		}

        header("Location: https://oauth.vk.com/authorize?client_id=".$this->clientId."&display=page&redirect_uri=http://anime-base.ru/Vk/code/&scope=".$this->scope."&response_type=code&v=5.62");
	}

    public function code () {

		if (!empty($this->user->vkId)) {

			header ("Location: /");
			return;

		}

        if (isset($_GET['error'])) exit($_GET['error']);

        if (isset($_GET['code'])) {

			$this->load->library('curl');
			$curl = $this->curl;

            $curl -> setUrl ("https://oauth.vk.com/access_token?client_id=".$this->clientId."&client_secret=".$this->securityKey."&redirect_uri=http://anime-base.ru/Vk/code/&code=".$_GET['code']);

            $data = json_decode ($curl -> getQuery (), true);

            if (isset($data['access_token'])) {

            	$curl -> setUrl ("https://api.vk.com/method/users.get?user_ids=".$data['user_id']."&fields=first_name,last_name,photo_100&access_token=".$data['access_token']);

	            $user = json_decode ($curl -> getQuery (), true)['response'][0];
	            $user['access_token'] = $data['access_token'];
	            $user['ip'] = $this->input->ip_address();

	            $this->load->model ("User/SignupModel");
	            $this->SignupModel->addByVk ($user);

				setcookie('sessionId', $data['access_token'], time()+86400*24, '/', '.anime-base.ru');
        	}

	        header ('Location: /');

        }
        else echo 'Invalid code!';

    }

}