<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">

<html itemscope itemtype="http://schema.org/WebPage" xmlns="http://www.w3.org/1999/xhtml" lang="ru">
<head>
    <title><?=((isset($title)) ? $title : 'Аниме база сериалов')?></title>
    <link rel="shortcut icon" href="/images/favicon.png" type="image/png" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width" />
    <meta name="description" content="<?=((isset($description)) ? $description : 'Ресурс для комфортного просмотра аниме. Собрана большая база японских сериалов. Публикуем новые серии каждый день, для вас!')?>"/>

    <script type="text/javascript">
        function dlOnload() {
          var jq = document.createElement("script"), mainScript;
          jq.src = "/js/jquery.min.js";
          document.body.appendChild(jq);

          jq.onload = function() {
            mainScript = document.createElement("script");
            mainScript.src = "/js/uikit.min.js";
            document.body.appendChild(mainScript);
          }
        }
        window.addEventListener("load", dlOnload, false);
    </script>
    
	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
	<script>
	  (adsbygoogle = window.adsbygoogle || []).push({
	    google_ad_client: "ca-pub-2093107717610748",
	    enable_page_level_ads: true
	  });
	</script>

    <style>
        body{visibility: hidden;}
    </style>
</head>
<body>
<nav class="uk-navbar-container" uk-navbar>

    <div class="uk-hidden@s uk-navbar-left" style="background: #47a3ff;">

        <a class="uk-navbar-item uk-logo" href="/" title="Аниме смотреть по жанрам"><span uk-icon="icon: youtube; ratio: 2"></span></a>

    </div>

    <div class="uk-hidden@s uk-navbar-right">

        <ul class="uk-navbar-nav">
                            <?=((($this->user->signin == false) or (isset($this->user->vkId) and empty($this->user->vkId))) ? '<li><a href="/Vk/signin"><span style="margin-bottom: -5px; margin-right: 7px;" uk-icon="icon: social" style="margin-right: 5px;"></span> Войти</a></li>' : '')?>
            <li><a href="/Search" title="Поиск аниме"><span uk-icon="icon: search"></span></a></li>

    <?php if (isset($this->user->avatar) and !empty($this->user->avatar)): ?>
            <li><a href="#" title="Смотреть аниме онлайн"><img class="uk-border-circle" style="height: 55px;" src="<?=$this->user->avatar?>"></a></li>
    <?php endif ?>

            <li><a href="#my-bar" title="Аниме в высоком качестве" uk-toggle style="background: #47a3ff;"><span uk-icon="icon: menu"></span></a></li>
        </ul>

    </div>


    <div class="uk-visible@s uk-navbar-left uk-width-1-4" style="background: #47a3ff;">

        <a class="uk-navbar-item uk-logo" title="Смотреть аниме в хорошем качестве" href="/"><span uk-icon="icon: youtube; ratio: 2"></span><span style="margin-left: 10px;">Аниме база</span></a>

    </div>

    <div class="uk-visible@s uk-navbar-left">

        <ul class="uk-navbar-nav">

                <?=((($this->user->signin == false) or (isset($this->user->vkId) and empty($this->user->vkId))) ? '<li><a href="/Vk/signin"><span style="margin-bottom: -5px; margin-right: 7px;" uk-icon="icon: social" style="margin-right: 5px;"></span> Войти через VK</a></li>' : '<li><a href="https://vk.com/anime_base_ru" target="_blank"><span style="margin-bottom: -5px; margin-right: 7px;" uk-icon="icon: comment" style="margin-right: 5px;"></span> Наша группа VK</a></li>')?>

                <?=((isset($_COOKIE['admin'])) ? '<li><a href="/Admin"><span uk-icon="icon: settings" style="margin-bottom: -5px; margin-right: 7px;"></span> Админка</a></li>' : '')?>

        </ul>

    </div>

    <div class="uk-visible@s uk-navbar-right">

        <div>
            <a class="uk-navbar-toggle" uk-search-icon title="Аниме поиск" href="#"></a>
            <div style="background: #4aa4ff;" class="uk-drop" uk-drop="mode: click; pos: left-center; offset: 0">
                <form action="/Search" method="GET" class="uk-search uk-search-navbar uk-width-1-1">

                     <div class="uk-inline uk-width-1-1">
                        <span class="uk-form-icon" uk-icon="icon: pencil" style="color: #ffffff"></span>
                        <input class="uk-search-input" name="name" type="search" value="" autofocus>
                    </div>
                    <input name="send" type="hidden">

                </form>
            </div>
        </div>

    <?php if (isset($this->user->avatar) and !empty($this->user->avatar)): ?>

        <ul class="uk-navbar-nav">
            <li><a href="#"><img class="uk-border-circle" style="height: 55px; margin-right: 15px;" src="<?=$this->user->avatar?>"> <?=$this->user->firstName?></a></li>
        </ul>

    <?php endif ?>

    </div>

</nav>

<div id="my-bar" uk-offcanvas>
    <div class="uk-offcanvas-bar">
        
    <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>

        <li><a href="" title="Главная страница аниме онлайн"><span class="uk-margin-small-right" uk-icon="icon: youtube"></span> Аниме база</a></li>

        <li class="uk-nav-header">Разделы сайта</li>
        <li><a href="/Anime/ongoing"><span class="uk-margin-small-right uk-text-success" uk-icon="icon: calendar" title="Аниме онгоинги"></span> Аниме Ongoing <span class="uk-float-right uk-label uk-label-success uk-text-small" style="font-size: .775rem;
    margin-top: 3px;"><?=$this->AnimeCount->ongoing ()?></span></a></li>
        <li><a href="/Top/today" title="Рейтинг аниме"><span class="uk-text-primary uk-margin-small-right" uk-icon="icon: bolt"></span> Рейтинг аниме</a></li>
        <li><a href="/Search" title="Искать аниме"><span class="uk-margin-small-right" uk-icon="icon: search"></span> Поиск аниме</a></li>
        <li><a href="https://vk.com/anime_base_ru" target="_blank"><span class="uk-margin-small-right" uk-icon="icon: social"></span> Мы Вконтакте</a></li>

<?php $animeList = $this->AnimeUserList->getUserList ($this->user->id); ?>
<?php if (isset($animeList[0])): ?>

         <li class="uk-nav-header">Отложенные</li>

      <?php foreach ($animeList as $key => $value): ?>

        <li>
        <a class="uk-text-truncate" title="Смотреть <?=$value->anime->name?>" href="/anime/<?=$value->anime->urlName?>"><span class="uk-margin-small-right" uk-icon="icon: future"></span> <?=$value->anime->name?></a></li>

        <?php endforeach ?>

<?php endif ?>

        <li class="uk-nav-header">TOP 5</li>
      <?php foreach ($this->TopModel->top () as $key => $value): ?>

        <li>
        <a class="uk-text-truncate" title="Смотреть онлайн <?=$value->name?>" href="/anime/<?=$value->urlName?>"><span class="uk-margin-small-right" uk-icon="icon: star"></span> <?=$value->name?></a></li>
        <?php endforeach ?>
        
        <li class="uk-nav-header">ТОП онгоинги</li>
      <?php foreach ($this->ongoing->top () as $key => $value): ?>

        <li>
        <a class="uk-text-truncate" title="Смотреть онгоинг <?=$value->name?> в хорошем качестве" href="/anime/<?=$value->urlName?>"><span class="uk-margin-small-right" uk-icon="icon: calendar"></span> <?=$value->name?></a></li>

        <?php endforeach ?>

    </ul>

    </div>
</div>

    <div class="nav-solid box-shadow-none uk-width-1-4 uk-visible@m" style="position: fixed;"></div>

    <div class="uk-grid-collapse" uk-grid>
        <div class="uk-visible@m uk-width-1-4" style="z-index: 3; position: relative;">

<div class="uk-card uk-card-default uk-card-body" style="    box-shadow: none;">

    <ul itemscope itemtype="http://www.schema.org/SiteNavigationElement" class="uk-nav-default uk-nav-parent-icon" uk-nav>


        <li class="uk-nav-header">Разделы сайта</li>
        <li itemprop="name"><a itemprop="url" href="http://<?=$_SERVER['SERVER_NAME']?>/Anime/ongoing" title="Аниме онгоинги <?=date ("Y")?>"><span class="uk-margin-small-right uk-text-success" uk-icon="icon: calendar"></span> Аниме Ongoing 
        <span class="uk-float-right uk-label uk-label-success uk-text-small" style="font-size: .775rem;
    margin-top: 3px;"><?=$this->AnimeCount->ongoing ()?></span>
        </a></li>
        <li itemprop="name"><a itemprop="url" href="http://<?=$_SERVER['SERVER_NAME']?>/Top/today" title="Аниме по рейтингу"><span class="uk-margin-small-right uk-text-primary" uk-icon="icon: bolt"></span> Рейтинг аниме</a></li>
        <li itemprop="name"><a itemprop="url" href="http://<?=$_SERVER['SERVER_NAME']?>/Search" title="Поиск аниме сериалов"><span class="uk-margin-small-right" uk-icon="icon: search"></span> Поиск</a></li>

        <li><a href="https://vk.com/anime_base_ru" target="_blank"><span class="uk-margin-small-right" uk-icon="icon: social"></span> Мы Вконтакте</a></li>

<?php if (isset($animeList[0])): ?>

         <li class="uk-nav-header">Отложенные</li>

      <?php foreach ($animeList as $key => $value): ?>

        <li>
        <a class="uk-text-truncate" title="Смотреть онлайн <?=$value->anime->name?>" href="/anime/<?=$value->anime->urlName?>"><span class="uk-margin-small-right" uk-icon="icon: future"></span> <?=$value->anime->name?></a></li>

        <?php endforeach ?>

<?php endif ?>

        <li class="uk-nav-header">TOP 5</li>
      <?php foreach ($this->TopModel->top () as $key => $value): ?>

        <li>
        <a class="uk-text-truncate link-top" title="Смотреть <?=$value->name?> в хорошем качестве" href="/anime/<?=$value->urlName?>"><span class="uk-margin-small-right" uk-icon="icon: star"></span> <?=$value->name?></a></li>

        <?php endforeach ?>

        <li class="uk-nav-header">ТОП онгоинги</li>
      <?php foreach ($this->ongoing->top () as $key => $value): ?>

        <li>
        <a class="uk-text-truncate link-ongoing" title="Смотреть онгоинг <?=$value->name?> в хорошем качестве" href="/anime/<?=$value->urlName?>"><span class="uk-margin-small-right" uk-icon="icon: calendar"></span> <?=$value->name?></a></li>

        <?php endforeach ?>

    </ul>

</div>

</div>

<div class="uk-width-1-1@s uk-width-3-4@m" style="padding: 15px;">

<?php 

if (isset($_COOKIE['admin'])) {

    $onlineUsers = $this->db->query ("SELECT `id` FROM `users` WHERE `lastTime` > '".(time()-250)."'")->num_rows ();

    echo '<div style="    background: #32d296;
    color: #ffffff;
    padding: 4px;
    font-size: 13px;
    border-radius: 2px 2px 0px 0px;">Статистика</div><div class="uk-card uk-card-default box-shadow-none uk-card-body uk-margin" style="    margin-top: 0 !important; padding:6px; font-size: 12px;">Онлайн <span class="uk-label uk-label-success uk-text-small" style="font-size: 11px;margin-left: 10px;">'.$onlineUsers.'</span>';
if ($onlineUsers > 1) {

	$onlineUser = $this->db->query ("SELECT `ip` FROM `users` WHERE `lastTime` > '".(time()-250)."'")->result ();
	echo '<br />';
	foreach ($onlineUser as $key => $value) {
		echo '<span class="uk-label uk-label-success uk-text-small" style="font-size: 11px;margin-left: 10px;">'.$value->ip.'</span>';
	}
	
}	

echo '</div>';


}

?>