
<div class="uk-card uk-card-default box-shadow-none uk-card-body uk-margin" style="margin-bottom: 0; padding: 10px 7px 0px 12px;">

<ul class="uk-breadcrumb" style="margin: 0 0 8px 0;">
    <li><span href="" class="uk-text-success"><span uk-icon="icon: star"></span> Аниме ТОП</span></li>
</ul>

</div>

      <div class="uk-grid-collapse" uk-grid style="background: #ffffff;">
      <?php foreach ($this->TopModel->top () as $key => $value): ?>

            <?php if ($key == 4):?>
                <?php break; ?>
            <?php endif ?>

            <div class="uk-width-1-2@m uk-width-1-1@s">

            <div>
            <a href="/anime/<?=$value->urlName?>" title="Смотреть онгоинг <?=$value->name?>" class="serie-link">
            <div uk-grid>
                <div class="uk-width-auto">
                    <div class="photo" style="width: 60px; height: 45px; background-image: url(<?=$value->image?>);"></div>
                </div>
                <div class="uk-width-expand uk-text-truncate">

                    <span uk-icon="icon: calendar"></span> <span style="font-size: 13px;"><?=$value->name?></span>

                    <div><div class="uk-text-small">
                        <span class="uk-text-success"><span uk-icon="icon: bolt"></span> <?=$value->producer?></span>
                    </div></div>

                </div>
            </div>
            </a>
            </div>

            </div>

    <?php endforeach ?>
    </div>
    <a class="link-place" title="Смотреть аниме топ" style="margin-bottom: 20px;" href="/Top"><span uk-icon="icon: grid"></span> Полный список</a>


<div class="uk-card uk-card-default box-shadow-none uk-card-body uk-margin" style="margin-bottom: 0; padding: 20px 20px 0px 30px;">

<ul class="uk-breadcrumb" itemscope="" itemtype="http://schema.org/BreadcrumbList" style="margin: 0 0 8px 0;">

    <li itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
        <a href="/" itemprop="item" title="Главная">
            <span itemprop="name">Главная</span>
            <meta itemprop="position" content="1">
        </a>
    </li>

    <li itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
        <span class="uk-text-success" href="#">
            <span uk-icon="icon: tag"></span> <span itemprop="name">Все жанры</span>
            <meta itemprop="position" content="2">
        </span>
    </li>

</ul>

<hr class="uk-divider-icon" style="margin-top: 0;"></div>

<div class="uk-grid-collapse" uk-grid style="background: #ffffff;">

<?php foreach ($genres as $key => $value): ?>

<div class="uk-width-1-2@m uk-width-1-1@s">

<div>
<a href="/Genres/view/id/<?=$value->id?>" title="Смотреть аниме <?=$value->name?>" class="serie-link">
<div uk-grid>
    <div class="uk-width-auto">
        <div class="photo" style="width: 120px; height: 60px; background-image: url(<?=$value->image?>);"></div>
    </div>
    <div class="uk-width-expand">

        <span uk-icon="icon: tag"></span> <?=$value->name?>

        <div><div class="uk-text-small uk-text-muted">
            <span uk-icon="icon: image"></span> <?=$value->animeTotal?> сериалов
        </div></div>

    </div>
</div>
</a>
</div>

</div>

<?php endforeach ?>

</div>

<h1 style="display: none;">Рейтинг аниме</h1>
