<script type="application/ld+json">
	{
	  "@context" : "http://schema.org",
	  "@type" : "WebSite", 
	  "name" : "Аниме база",
	  "url" : "http://anime-base.ru/",
	  "potentialAction" : {
	    "@type" : "SearchAction",
	    "target" : "http://animie-base.ru/Search?name={search_term}",
	    "query-input" : "required name=search_term"
	  }                     
	}
</script>

<div class="uk-card uk-card-default box-shadow-none uk-card-body" style="margin-bottom: 0px !important; padding: 15px; border-bottom: 1px solid #f3f3f3;">
<ul class="uk-breadcrumb" itemscope="" itemtype="http://schema.org/BreadcrumbList">

    <li itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
        <span itemprop="name">Главная страница</span>
        <meta itemprop="position" content="1">
    </li>

</ul>

</div>

<div class="uk-card box-shadow-none uk-card-default uk-card-body" style="margin-bottom: 15px;">
    <h1>Аниме база</h1>
    <p style="font-size: 14px;"><span class="uk-text-success">AnimeBase.ru</span> - огромная <strong>база по просмотру аниме</strong>. Этот ресурс был создан специально, для комфортного просмотра аниме. Мы собрали для вас большую <strong>базу аниме сериалов</strong> разных жанров и режисеров. Работаем каждый день, что бы публиковать для вас <strong>новые серии</strong>, что бы вы первыми могли <strong>смотреть аниме новинки</strong> <?=date ("Y")?> года. 

	<form class="uk-grid-small uk-grid-collapse" action="/Search" method="GET" uk-grid>
	    <div class="uk-width-3-4">
	        <input class="uk-input uk-width-1-1" name="name" type="text" placeholder="Поиск аниме">
	    </div>
	    <div class="uk-width-1-4">
	        <button class="uk-button uk-button-default uk-width-1-1" style="border-left: none;"><span uk-icon="icon: search"></span></button>
	    </div>
	</form>

    </p>

</div>

<!--
<div class="uk-card box-shadow-none uk-card-default uk-card-body" style="border-top: 1px solid #f1f1f1; margin-bottom: 15px;">

	<div class="uk-child-width-expand@s uk-text-center uk-text-success" uk-grid>
	    <div>
	        <div class="uk-text-uppercase"><strong>смотрели аниме</strong></div>
	        <span class="uk-text-lead" style="font-size: 45px;">127</span><br /><span class="uk-text-muted">человека</span>
	    </div>
	    <div>
	        <div class="uk-text-uppercase"><strong>Добавлено</strong></div>
	        <span class="uk-text-lead" style="font-size: 45px;">7</span><br /><span class="uk-text-muted">сериалов сегодня</span>
	    </div>
	    <div>
	        <div class="uk-text-uppercase"><strong>Понравилось</strong></div>
	        <span class="uk-text-lead" style="font-size: 45px;">58</span><br /><span class="uk-text-muted">людям</span>
	    </div>
	</div>

</div>
-->

<div class="uk-card box-shadow-none uk-card-default uk-card-body uk-alert-success" style="margin-bottom: 0px !important; padding: 15px; border-bottom: 1px solid #f3f3f3;">

<span class="uk-float-right"><a class="uk-button uk-button-text" href="/Anime/ongoing">Все <span uk-icon="icon: chevron-right" class="uk-icon"></span></a></span>

<h2 style="margin-bottom: 0px !important;">Аниме сериалы <?=date ("Y")?></h2>

<?php 

	$date = date("n");

	if ($date == 12 or $date == 1 or $date == 2) $season = 'зимнего';
	if ($date == 3 or $date == 4 or $date == 5) $season = 'весеннего';
	if ($date == 6 or $date == 7 or $date == 8) $season = 'летнего';
	if ($date == 9 or $date == 10 or $date == 11) $season = 'осеннего';

?>

<div class="uk-text-small">Новинки <?=$season?> сезона</div>
</div>


    <div class="uk-grid-collapse uk-card box-shadow-none" uk-grid style="background: #ffffff;">
	<?php foreach ($anime as $key => $value): ?>
			<div class="uk-width-1-2@m uk-width-1-1@s">

			<div itemscope itemtype="http://schema.org/Movie">
			<a href="/anime/<?=$value->urlName?>" title="Аниме <?=$value->name?>" class="serie-link" style="padding: 15px 15px 15px 15px;">
			<div uk-grid class="uk-grid-collapse">
			    <div class="uk-width-auto" style="padding-right: 10px;">
			        <div class="photo" style="width: 50px; height: 50px; background-image: url(<?=$value->imageSmall?>);"></div>
			        <img itemprop="image" style="display: none;" src="<?=$value->imageSmall?>">
			    </div>
			    <div class="uk-width-expand uk-text-truncate">

			        <span uk-icon="icon: play"></span> <span itemprop="name" style="font-size: 14px;"><?=$value->name?></span>

			        <div><div style="font-size: 12px; text-transform: none;">
			            <span class="uk-text-muted">
			            	<?=$value->year?> год,
			            	<?php if ($value->season == 1): ?>
                                        Зимний
                                    <?php elseif ($value->season == 2):?>
                                        Весенний
                                    <?php elseif ($value->season == 3):?>
                                        Летний
                                    <?php else:?>
                                        Осенний
                                    <?php endif ?>
                                        сезон
                                        <br />
			            	Режиссер <span itemprop="director" itemscope itemtype="http://schema.org/Person"><span itemprop="name" class="uk-text-success"><?=$value->producer?></span></span>
			            	<br />
			            	<span class="uk-label uk-label-success uk-text-small" style="font-size: 11px;"><?=$value->realSeries?> <span style="text-transform: none;">из</span> <?=$value->amountSeries?></span> серий
			            </span>
			        </div></div>

			    </div>
			</div>
			</a>
			</div>

			</div>

    <?php endforeach ?>
    </div>
	<a class="box-shadow-none link-place" title="Смотреть аниме топ" style="margin-bottom: 15px; border: none;" href="/Anime/ongoing"><span uk-icon="icon: chevron-down" class="uk-icon"></span> Полный список</a>

<div class="uk-card box-shadow-none uk-card-default uk-card-body uk-alert-success" style="margin-bottom: 0px !important; padding: 15px; border-bottom: 1px solid #f3f3f3;">

<span class="uk-float-right"><a class="uk-button uk-button-text" href="/Top">Все <span uk-icon="icon: chevron-right" class="uk-icon"></span></a></span>

<h3 style="margin-bottom: 0px !important;">Список аниме</h3>
<div class="uk-text-small">Случайные сериалы</div>
</div>

    <div class="uk-grid-collapse uk-card box-shadow-none" uk-grid style="background: #ffffff;">
	<?php foreach ($this->TopModel->rand (10) as $key => $value): ?>

			<div class="uk-width-1-2@m uk-width-1-1@s">

			<div itemscope itemtype="http://schema.org/Movie">
			<a href="/anime/<?=$value->urlName?>" title="Аниме <?=$value->name?>" class="serie-link" style="padding: 15px 15px 15px 15px;">
			<div uk-grid class="uk-grid-collapse">
			    <div class="uk-width-auto" style="padding-right: 10px;">
			        <div class="photo" style="width: 50px; height: 50px; background-image: url(<?=$value->imageSmall?>);"></div>
			        <img itemprop="image" style="display: none;" src="<?=$value->imageSmall?>">
			    </div>
			    <div class="uk-width-expand uk-text-truncate">

			        <span uk-icon="icon: play"></span> <span itemprop="name" style="font-size: 14px;"><?=$value->name?></span>

			        <div><div style="font-size: 12px; text-transform: none;">
			            <span class="uk-text-muted">
			            	<?=$value->year?> год,
			            	<?php if ($value->season == 1): ?>
                                        Зимний
                                    <?php elseif ($value->season == 2):?>
                                        Весенний
                                    <?php elseif ($value->season == 3):?>
                                        Летний
                                    <?php else:?>
                                        Осенний
                                    <?php endif ?>
                                        сезон
                                        <br />
			            	Режиссер <span itemprop="director" itemscope itemtype="http://schema.org/Person"><span itemprop="name" class="uk-text-success"><?=$value->producer?></span></span>
			            	<br />
			            	<span class="uk-label uk-label-success uk-text-small" style="font-size: 11px;"><?=$value->amountSeries?></span> серий
			            </span>
			        </div></div>

			    </div>
			</div>
			</a>
			</div>

			</div>

    <?php endforeach ?>
    </div>
	<a class="box-shadow-none link-place" title="Смотреть аниме топ" style="margin-bottom: 15px; border: none;" href="/Top"><span uk-icon="icon: chevron-down" class="uk-icon"></span> Полный список</a>