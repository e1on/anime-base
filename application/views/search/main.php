<div class="uk-card uk-card-default box-shadow-none uk-card-body uk-margin" style="margin-bottom: 0px !important; padding: 15px; border-bottom: 1px solid #f3f3f3;">
<ul class="uk-breadcrumb" itemscope="" itemtype="http://schema.org/BreadcrumbList">

    <li itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
        <a href="/" itemprop="item" title="Главная">
            <span itemprop="name">Главная</span>
            <meta itemprop="position" content="1">
        </a>
    </li>

    <li itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
        <span class="uk-text-success" href="#">
            <span uk-icon="icon: search"></span> <span itemprop="name">Поиск аниме</span>
            <meta itemprop="position" content="2">
        </span>
    </li>

</ul>



<?php if (isset($titleVoice[0])): ?>

	<?php foreach ($titleVoice as $key => $value):?>
		<mark><span class="uk-text-small uk-text-muted"><?=$value?></span></mark>
	<?php endforeach ?>

<?php endif ?>

<?php if (isset($titleGenre[0])): ?>

	<?php foreach ($titleGenre as $key => $value):?>
		<mark><span class="uk-text-small uk-text-muted"><?=$value?></span></mark>
	<?php endforeach ?>

<?php endif ?>

</div>

<div class="uk-card uk-card-default uk-card-body box-shadow-none" style="padding: 20px 30px;">
	<form action="" method="GET">
		<fieldset class="uk-fieldset">

			<div class="uk-grid-collapse uk-margin" uk-grid>

				<div class="uk-width-3-4">

					<div class="uk-inline uk-width-1-1">
						<a class="uk-form-icon" href="#" uk-icon="icon: pencil"></a>
						<input class="uk-width-1-1 uk-input" name="name" type="text" style="border-right: 0;" <?=(isset($inputNameValue) ? 'value="'.$inputNameValue.'"' : '')?> placeholder="Название аниме...">
					</div>

				</div>

				<div class="uk-width-1-4">
					<button name="send" class="uk-width-1-1  uk-button uk-button-primary"><span uk-icon="icon: search"></span></button>
				</div>

			</div>

			<div class="uk-grid-collapse" uk-grid>
				<div class="uk-width-1-3">
					<select class="uk-select" name="sort" style="border-right: 0;">
						<option value="1">По рейтингу</option>
						<option value="2">По году выхода</option>
					</select>
				</div>
				<div class="uk-width-1-3">

					<div class="uk-inline  uk-width-1-1">
					<button class="uk-button uk-button-default uk-width-1-1" type="button" style="border-right: 0;">Жанры</button>
						<div uk-drop>
							<div class="uk-panel uk-panel-scrollable" style="background: #ffffff;">
							<ul class="uk-list">
							<?php foreach ($genres as $key => $value): ?>
                                <li>
                                	<label>
                                		<input class="uk-checkbox" type="checkbox" name="genres[]" value="<?=$value->id?>"> <?=$value->name?>
                                	</label>
                                </li>
                            <?php endforeach ?>
							</ul>
							</div>
						</div>
					</div>

				</div>
				<div class="uk-width-1-3">

					<div class="uk-inline  uk-width-1-1">
					<button class="uk-button uk-button-default uk-width-1-1" type="button">Озвучил</button>
						<div uk-drop>
							<div class="uk-panel uk-panel-scrollable" style="background: #ffffff;">
							<ul class="uk-list">
							<?php foreach ($voices as $key => $value): ?>
                                <li>
                                	<label>
                                		<input class="uk-checkbox" type="checkbox" name="voices[]" value="<?=$value->id?>"> <?=$value->name?>
                                	</label>
                                </li>
                            <?php endforeach ?>
							</ul>
							</div>
						</div>
					</div>

				</div>
			</div>

		</fieldset>
	</form>
</div>

<p></p>

<?php if (isset ($anime)): ?>

	<?php if ($anime == 0): ?>

		<div class="uk-card uk-card-default box-shadow-none uk-card-body uk-margin" style="margin-top: 0 !important; border-top: 1px solid #f0506e;"><span class="uk-text-danger"><span uk-icon="icon: info"></span> К сожалению, поиск по сайту не дал никаких результатов.</span> <span class="uk-text-muted">Попробуйте изменить или сократить Ваш запрос.</span></div>

	<?php else: ?>

		<?php foreach ($anime as $key => $value): ?>

			<?php $this->load->view ("anime/widget/animeLittle", ['value' => $value]) ?>

		<?php endforeach ?>

		<?=$page_links?>

	<?php endif ?>

<?php endif ?>