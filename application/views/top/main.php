<div class="uk-card uk-card-default box-shadow-none uk-card-body uk-margin" style="margin-bottom: 0px !important; padding: 15px; border-bottom: 1px solid #f3f3f3;">
<ul class="uk-breadcrumb" itemscope="" itemtype="http://schema.org/BreadcrumbList">

    <li itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
        <a href="/" itemprop="item" title="Главная">
            <span itemprop="name">Главная</span>
            <meta itemprop="position" content="1">
        </a>
    </li>

    <li itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
        <span class="uk-text-success" href="#">
            <span itemprop="name">Рейтинг аниме</span>
            <meta itemprop="position" content="2">
        </span>
    </li>

    <li itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
        <span class="uk-text-success" href="#">
            <span uk-icon="icon: youtube"></span> <span itemprop="name"><?=(($action == 'index' ? 'Общий ТОП' : ($action == 'today' ? 'За сутки' : 'За неделю')))?></span>
            <meta itemprop="position" content="3"
        ></span>
    </li>

</ul>

</div>

<div class="uk-card box-shadow-none uk-card-default uk-card-body" style="margin-bottom: 15px; padding-bottom: 15px;">

<div class="uk-grid uk-grid-collapse">
    <div class="uk-width-1-3">

    <a href="<?=(($action == 'index') ? '#' : '/Top')?>" title="Аниме рейтинги" class="uk-button uk-button-default uk-width-1-1 <?=(($action == 'index') ? 'uk-button-primary' : '')?> uk-text-center"><span uk-icon="icon: star"></span> <small class="uk-visible@s">Общий</small></a>

    </div>
    <div class="uk-width-1-3">

    <a href="<?=(($action == 'togay') ? '#' : '/Top/today')?>" title="Аниме топ за сутки" class="uk-button uk-button-default uk-width-1-1 <?=(($action == 'today') ? 'uk-button-primary' : '')?> uk-text-center"><span uk-icon="icon: history"></span> <small class="uk-visible@s">За сутки</small></a>

    </div>
    <div class="uk-width-1-3">

    <a href="<?=(($action == 'week') ? '#' : '/Top/week')?>" title="Аниме топ за неделю"  class="uk-button uk-button-default uk-width-1-1 <?=(($action == 'week') ? 'uk-button-primary' : '')?> uk-text-center"><span uk-icon="icon: calendar"></span>  <small class="uk-visible@s">За неделю</small></a>

    </div>

</div>

</div>


<p>

<?php foreach ($anime as $key => $value): ?>

	<?php $this->load->view ("anime/widget/animeLittle", ['value' => $value]) ?>

<?php endforeach ?>

<?=$page_links?>

