<div class="uk-card uk-card-default box-shadow-none uk-card-body uk-margin" style="margin-bottom: 0px !important; padding: 15px; border-bottom: 1px solid #f3f3f3;">
<ul class="uk-breadcrumb" itemscope="" itemtype="http://schema.org/BreadcrumbList">

    <li itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
        <a href="/" itemprop="item" title="Главная">
            <span itemprop="name">Главная</span>
            <meta itemprop="position" content="1">
        </a>
    </li>

    <li itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
        <span class="uk-text-success" href="#">
            <span itemprop="name">Аниме онгоинги</span>
            <meta itemprop="position" content="2">
        </span>
    </li>

</ul>

</div>

<?php if ($this->paginator->_page < 2): ?>
	
<div class="uk-card box-shadow-none uk-card-default uk-card-body" style="margin-bottom: 15px; padding-bottom: 15px;">
    <h1>База <?=date ("Y")?></h1>
    <p style="font-size: 14px;" style="padding-bottom: 0px;">Представляем вам, <strong>аниме <?=date ("Y")?></strong> года, смотрите в любимой озвучке от любимых даберов. Мы трудимся для вас каждый день, публикуя сериалы и <strong>новые серии</strong> к ним. Предлогаем воспользоватся поиском, если ищите <strong>аниме онгоинг</strong>. Или подобрать из списка новинок - ниже.

	<form class="uk-grid-small uk-grid-collapse" action="/Search" method="GET" uk-grid>
	    <div class="uk-width-3-4">
	        <input class="uk-input uk-width-1-1" name="name" type="text" placeholder="Поиск аниме">
	    </div>
	    <div class="uk-width-1-4">
	        <button class="uk-button uk-button-default uk-width-1-1" style="border-left: none;"><span uk-icon="icon: search"></span></button>
	    </div>
	</form>

    </p>

</div>

<div class="uk-card box-shadow-none uk-card-default uk-card-body uk-alert-success" style="margin-bottom: 0px !important; padding: 15px; border-bottom: 1px solid #f3f3f3;">

<h2 style="margin-bottom: 0px !important;">Новые серии аниме</h2>

</div>

    <div class="uk-card box-shadow-none uk-grid-collapse" uk-grid style="background: #ffffff; margin-bottom: 15px;">
	<?php foreach ($ongoingSeries as $key => $value): ?>
      		<?php if ($key == 6):?>
      			<?php break; ?>
      		<?php endif ?>
			<div class="uk-width-1-2@m uk-width-1-1@s">

			<div itemscope itemtype="http://schema.org/Movie">
			<a href="/anime/<?=$value->anime->urlName?>" title="Аниме <?=$value->anime->name?>" class="serie-link" style="padding: 8px; border: 0;">
			<div uk-grid class="uk-grid-collapse">
			    <div class="uk-width-auto" style="padding-right: 10px;">
			        <div class="photo" style="width: 50px; height: 50px; background-image: url(<?=$value->anime->imageSmall?>);"></div>
			        <img itemprop="image" style="display: none;" src="<?=$value->anime->imageSmall?>">
			    </div>
			    <div class="uk-width-expand uk-text-truncate">

			        <span uk-icon="icon: play"></span> <span itemprop="name" style="font-size: 14px;"><?=$value->anime->name?></span>

			        <div><div style="font-size: 12px; text-transform: none;">
			            <span class="uk-text-muted">
			            	<span class="uk-label uk-label-success uk-text-small" style="font-size: 11px;"><?=$value->title?></span> добавлена <?=$value->timeAdd?>
			            </span>
			        </div></div>

			    </div>
			</div>
			</a>
			</div>

			</div>

    <?php endforeach ?>
    </div>

<?php endif ?>

<div class="uk-card box-shadow-none uk-card-default uk-card-body uk-alert-success" style="margin-bottom: 0px !important; padding: 15px; border-bottom: 1px solid #f3f3f3;">

<span class="uk-float-right">По дате выхода <span uk-icon="icon: chevron-down" class="uk-icon"></span></span>

<h3 style="margin-bottom: 0px !important;">Аниме сериалы <?=date ("Y")?></h3>

<?=($this->paginator->_page > 1 ? '<div class="uk-text-small">Страница '.$this->paginator->_page.'</div>' : '')?>

</div>

<?php foreach ($anime as $key => $value): ?>

        <?php $this->load->view ("anime/widget/animeShort", ['value' => $value]) ?>

<?php endforeach ?>

<?=$page_links?>