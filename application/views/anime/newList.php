
<div class="uk-card uk-card-default box-shadow-none uk-card-body uk-margin" style="margin-bottom: 0; padding: 20px 20px 0px 30px;">

<ul class="uk-breadcrumb" itemscope="" itemtype="http://schema.org/BreadcrumbList" style="margin: 0 0 8px 0;">

    <li itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
        <a href="/" itemprop="item" title="Главная">
            <span itemprop="name">Главная</span>
            <meta itemprop="position" content="1">
        </a>
    </li>

    <li itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
        <span class="uk-text-success" href="#">
            <span uk-icon="icon: bell"></span> <span itemprop="name">Свежие аниме</span>
            <meta itemprop="position" content="2">
        </span>
    </li>

</ul>

<hr class="uk-divider-icon" style="margin-top: 0;"></div>

<?php foreach ($anime as $key => $value): ?>

        <?php $this->load->view ("anime/widget/animeLittle", ['value' => $value]) ?>

<?php endforeach ?>

<?=$page_links?>

<h1 style="display: none;">Новые</h1>
