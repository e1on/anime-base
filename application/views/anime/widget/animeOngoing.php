<div class="uk-card uk-card-default uk-card-body box-shadow-none">
    <div class="uk-grid-collapse" itemscope itemtype="http://schema.org/Movie" uk-grid>

        <div class="uk-width-1-4">
        <a href="/Anime/view/id/<?=$value->id?>" title="Смотреть онлайн <?=$value->name?>">
<div class="uk-inline">
    <img itemprop="image" class="uk-border-rounded" alt="Смотреть онлайн <?=$value->name?>" src="<?=$value->image

    ?>">
    <div class="uk-position-top-left uk-overlay uk-overlay-default ongoing-img"><span uk-icon="icon: calendar;" class="uk-visible@s"></span> Ongoing</div>
</div>
</a>
        </div>

        <div class="uk-width-3-4" style="padding-left: 10px;">

                    <ul class="uk-subnav uk-subnav-pill" style="display: inline-block;  margin: 0; margin-left: -20px;">
                        <li><a href="/anime/<?=$value->urlName?>" class="uk-text-primary" title="Смотреть онлайн <?=$value->name?> в высоком качестве"><span itemprop="name"><span uk-icon="icon: play;"></span>  <?=$value->name?></span></a></li>
                    </ul>
               
                    <ul class="uk-subnav uk-subnav-pill uk-text-right" style="display: inline-block;  margin: 0; margin-left: -20px;">
                        <li><a href="/anime/<?=$value->urlName?>" class="uk-text-success" title="Смотреть высоком качестве <?=$value->name?>"><?=$value->realSeries?> из <?=$value->amountSeries?> эпизодов</a></li>
                    </ul>

<table class="uk-table uk-table-striped" style="font-size: 13px;">
    <tbody>
        <tr>
            <td class="uk-visible@s"><span class="uk-text-muted">Год</span></td>
            <td>
<span class="uk-text-success">
<meta itemprop="datePublished" content="<?=date ("Y-m-d", $value->time)?>">
<span uk-icon="icon: calendar"></span> <?=$value->year?>,
                                    <?php if ($value->season == 1): ?>
                                        Зимний
                                    <?php elseif ($value->season == 2):?>
                                        Весенний
                                    <?php elseif ($value->season == 3):?>
                                        Летний
                                    <?php else:?>
                                        Осенний
                                    <?php endif ?>
                                        сезон</span>
            </td>
        </tr>
        <tr>
            <td class="uk-visible@s"><span class="uk-text-muted">Жанр</span></td>
            <td>
                                    <span class="uk-text-success"><?php foreach ($value->genre as $key2 => $value2): ?>

                                        <a class="anime-list-link" href="/Search?name=&send=&sort=1&genres[]=<?=$value2['id']?>" title="Список аниме <?=$value2['name']?>"><span uk-icon="icon: tag"></span> <span itemprop="genre"><?=$value2['name']?></span></a>

                                    <?php endforeach ?></span>
            </td>
        </tr>
        <tr>
            <td class="uk-visible@s"><span class="uk-text-muted">Озвучивание</span></td>
            <td>
                                   <span class="uk-text-success"> <?php foreach ($value->voice as $key2 => $value2): ?>

                                         <a class="anime-list-link" href="/Search?name=&send=&sort=1&voices[]=<?=$value2['id']?>" title="Аниме озвучка <?=$value2['name']?>"><span uk-icon="icon: play"></span>  <?=$value2['name']?></a>

                                    <?php endforeach ?></span>
            </td>
        </tr>
        <tr>
            <td class="uk-visible@s"><span class="uk-text-muted">Режиссёр</span></td>
            <td>
            <span itemprop="director" itemscope itemtype="http://schema.org/Person">
            <span itemprop="name" class="uk-text-success"><span uk-icon="icon: bolt"></span> <?=((empty($value->producer)) ? 'неизвестно' : $value->producer)?></span></span></td>
        </tr>
    </tbody>
</table>

 
        </div> 
        
<div itemprop="description" style="display: none;"><?=$value->description?></div>

<?php

$description = strip_tags($value->description);
$description = substr($description, 0, 900);
$description = rtrim($description, "!,.-");
$description = substr($description, 0, strrpos($description, ' ')).'...';

        ?>

                    <div class="uk-text-primary" style="font-size: 12px; margin-top: 10px;"><?=$description?></div>
    </div>
    </div>
    <a class="link-place" title="Скачать торрент <?=$value->name?>" style="margin-bottom: 20px;" href="/anime/<?=$value->urlName?>"><span uk-icon="icon: youtube"></span> Смотреть</a>
<!--
    <?php
    $rand = mt_rand(2, 10);
    ?>
    
    <?php if ($rand == 5): ?>

<div class="uk-card uk-card-secondary uk-card-body box-shadow-none">
    <div class="uk-grid-collapse" uk-grid>

        <div class="uk-width-1-4">
        <img class="uk-border-rounded" src="/images/tera.jpg">
        </div>

        <div class="uk-width-3-4" style="padding-left: 10px;">

                    <ul class="uk-subnav uk-subnav-pill" style="display: inline-block;  margin: 0; margin-left: -20px;">
                        <li><a href="https://apygame.com/click/58a8657f8b30a879018b4576/111718/160290/subaccount" class="uk-text-primary" style="color: #ffffff !important;"><span uk-icon="icon: play;"></span> TERA ONLINE / Тера Онлайн</a></li>
                    </ul>
               
                    <ul class="uk-subnav uk-subnav-pill uk-text-right" style="display: inline-block;  margin: 0; margin-left: -20px;">
                        <li><a href="https://apygame.com/click/58a8657f8b30a879018b4576/111718/160290/subaccount" style="color: #ffffff !important;" class="uk-text-primary"><span uk-icon="icon: desktop;"></span></a></li>
                    </ul>

<ul class="uk-list uk-text-success" style="font-size: 13px; margin-left: 10px;">
    <li><span uk-icon="icon: world"></span> Огромный красочный мир</li>
    <li><span uk-icon="icon: git-branch"></span> Новый этап в развитии ММО</li>
    <li class="uk-text-uppercase"><span uk-icon="icon: check"></span> Подарки при регистрации</li>
</ul>

        </div> 


                    <div class="uk-text-primary" style="color: #ffffff !important; font-size: 12px; margin-top: 10px;">Вас ждет незабываемое путешествие по 
неизведанным землям волшебного мира Арбореи! 
Вы встретите новых друзей, научитесь сражаться с 
могущественными монстрами и узнаете множество секретов, 
которые таит в себе TERA: THE NEXT! 
Спешите к новым завоеваниям и подвигам!

Мы ждем вас!</div>
    </div>
    </div>
    <a class="link-place" style="margin-bottom: 20px; background: #222222; color: #32d296; ffffff; border-top: 1px solid #2bd287;" href="https://apygame.com/click/58a8657f8b30a879018b4576/111718/160290/subaccount"><span uk-icon="icon: cloud-download"></span> Играть</a>

    <?php endif ?>

    -->