<?php 

if (date ("n") == 12 or date ("n") == 1 or date ("n") == 2) $season = 1;
if (date ("n") == 3 or date ("n") == 4 or date ("n") == 5) $season = 2;
if (date ("n") == 6 or date ("n") == 7 or date ("n") == 8) $season = 3;
if (date ("n") == 9 or date ("n") == 10 or date ("n") == 11) $season = 4;

if ($value->year == date("Y") and $value->season >=($season-1)) {
    $markerText = '<span uk-icon="icon: calendar;" class="uk-visible@s"></span>Ongoing';
    $class = 'ongoing-img-mark';

}
else {
    $markerText = '<span uk-icon="icon: star;" class="uk-visible@s"></span> FULL';
    $class = 'ongoing-img-mark full-img';

}

$description = strip_tags($value->description);
$description = substr($description, 0, 300);
$description = rtrim($description, "!,.-");
$description = substr($description, 0, strrpos($description, ' ')).'...';

?>

<div class="uk-card uk-card-default box-shadow-none" style="margin-bottom: 15px;">
    
    <div class="uk-transition-toggle uk-grid-collapse" uk-grid>
        <div class="uk-width-1-3 uk-visible@s">



            <div class="uk-text-center">
                <div class="uk-inline-clip uk-light">
                    <img itemprop="image" class="img-anime-avatar" src="<?=$value->imageLow?>" alt="<?=$value->name?>">
                    <div class="uk-position-center">
                        <span class="uk-transition-fade" uk-icon="icon: play; ratio: 5"></span>
                    </div>
                <div class="uk-text-success uk-overlay uk-overlay-primary uk-position-bottom" style="padding: 5px; border-radius: 0px 0px 0px 3px;">
                    <div class="uk-text-bold" style="font-size: 18px;"><?=$value->realSeries?> из <?=$value->amountSeries?></div>
                    <div class="uk-text-small">эпизодов</div>
                </div>
                </div>
            </div>

            <div class="uk-position-top-left uk-overlay uk-overlay-default <?=$class?>"><?=$markerText?></div>

        </div>
        <div class="uk-width-expand@s">
            <div class="uk-card-body">

                <article class="uk-comment uk-hidden@s">
                    <header class="uk-comment-header uk-grid-medium uk-grid-collapse uk-flex-middle" uk-grid>
                        <div class="uk-width-auto">
                            <img class="img-anime-avatar" src="<?=$value->imageSmall?>" width="80" alt="<?=$value->name?>">
                        </div>
                        <div class="uk-width-expand">
                                <ul class="uk-subnav uk-subnav-pill" style="display: inline-block;  margin: 0; margin-left: -10px; margin-bottom: 8px;">
                                    <li><a href="/anime/<?=$value->urlName?>" title="Смотреть онлайн <?=$value->name?> в высоком качестве"><span><span uk-icon="icon: play;"></span>  <?=$value->name?></span></a></li>
                                </ul>

                                <ul class="uk-subnav uk-subnav-pill uk-text-right" style="display: inline-block;  margin: 0; margin-left: -10px;">
                                    <li><a href="/anime/<?=$value->urlName?>" style="color: #3399ff!important;" title="Смотреть высоком качестве <?=$value->name?>"><?=$value->realSeries?> из <?=$value->amountSeries?> эпизодов</a></li>
                                </ul>

                            </ul>
                        </div>
                    </header>
                </article>


                <ul class="uk-visible@s uk-subnav uk-subnav-pill" style="display: inline-block;  margin: 0; margin-left: -20px; margin-bottom: 8px;">
                    <li><a href="/anime/<?=$value->urlName?>" title="Смотреть онлайн <?=$value->name?> в высоком качестве"><span itemprop="name"><span uk-icon="icon: play;"></span>  <?=$value->name?></span></a></li>
                </ul>

                <div><div class="anime-date uk-text-small">

                    <meta itemprop="datePublished" content="<?=date ("Y-m-d", $value->time)?>">

                    <span uk-icon="icon: calendar"></span> <?=$value->year?>,
                    <?php if ($value->season == 1): ?>
                        Зимний
                    <?php elseif ($value->season == 2):?>
                        Весенний
                    <?php elseif ($value->season == 3):?>
                        Летний
                    <?php else:?>
                        Осенний
                    <?php endif ?>
                        сезон
                </div></div>

                <div class="uk-text-small">
                <?php foreach ($value->genre as $key2 => $value2): ?>

                    <a class="anime-list-link" href="/Search?name=&send=&sort=1&genres[]=<?=$value2['id']?>" title="Аниме <?=$value2['name']?>"><span uk-icon="icon: tag; ratio: 0.8"></span> <span itemprop="genre"><?=$value2['name']?></span></a>

                <?php endforeach ?>
                </div>

                <div class="uk-text-small">
                <?php foreach ($value->voice as $key2 => $value2): ?>

                    <a class="anime-list-link anime-list-link-dub" href="/Search?name=&send=&sort=1&voices[]=<?=$value2['id']?>" title="Озвучка <?=$value2['name']?>"><span uk-icon="icon: play; ratio: 0.8"></span> <?=$value2['name']?></a>

                <?php endforeach ?>
                </div>

                <div class="anime-date uk-text-small">

                    <span itemprop="director" itemscope itemtype="http://schema.org/Person">

                        <span itemprop="name"><span uk-icon="icon: bolt; ratio: 0.8"></span> <?=((empty($value->producer)) ? 'неизвестно' : $value->producer)?></span></span>

                </div>

                <div class="uk-text-primary" style="font-size: 12px; margin-top: 10px;"><?=$description?></div>

            </div>

        </div>
    </div>

</div>