<?php 
$value = $anime;
if (date ("n") == 12 or date ("n") == 1 or date ("n") == 2) $season = 1;
if (date ("n") == 3 or date ("n") == 4 or date ("n") == 5) $season = 2;
if (date ("n") == 6 or date ("n") == 7 or date ("n") == 8) $season = 3;
if (date ("n") == 9 or date ("n") == 10 or date ("n") == 11) $season = 4;

if ($value->year == date("Y") and $value->season >=($season-1)) {
    $markerText = '<span uk-icon="icon: calendar;" class="uk-visible@s"></span>Ongoing';
    $class = 'ongoing-img-mark';

    $folderLink = '/Anime/ongoing';
    $folderName = 'Онгоинги';

}
else {
    $markerText = '<span uk-icon="icon: star;" class="uk-visible@s"></span> FULL';
    $class = 'ongoing-img-mark full-img';

    $folderLink = '/Top/today';
    $folderName = 'Аниме';

}

$description = $value->description;

?>

<?=(isset($setAnime) ? '
<div uk-alert class="uk-alert-success">
    <a class="uk-alert-close" uk-close></a>
    <p>Вы добавили это аниме в ваш список <a class="uk-button uk-button-text" href="#">отложенных просмотров</a>!</p>
</div>
' : '')?>

<?=(isset($setRecall) ? '
<div uk-alert class="uk-alert-success">
    <a class="uk-alert-close" uk-close></a>
    <p>Мнение добавлено</a>!</p>
</div>
' : '')?>

<?php 

$str=strpos($anime->name, "/");
$name=substr($anime->name, 0, $str);

?>


<div class="uk-card uk-card-default box-shadow-none uk-card-body uk-margin" style="margin-bottom: 15px !important; padding: 15px; border-bottom: 1px solid #f3f3f3;">
<ul class="uk-breadcrumb" itemscope="" itemtype="http://schema.org/BreadcrumbList" style="margin: 0 0 0px 0;">

    <li itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
        <a href="/" itemprop="item" title="Главная">
            <span itemprop="name">Главная</span>
            <meta itemprop="position" content="1">
        </a>
    </li>

    <li itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
        <a href="<?=$folderLink?>">
            <span itemprop="name"><?=$folderName?></span>
            <meta itemprop="position" content="2"
        ></a>
    </li>

    <li itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
        <span class="uk-text-success" href="#">
            <span uk-icon="icon: youtube"></span> <span itemprop="name"><?=$name?></span>
            <meta itemprop="position" content="3"
        ></span>
    </li>

</ul>

</div>

<div class="uk-card uk-card-default box-shadow-none" style="margin-bottom: 15px;">
    
    <div class="uk-transition-toggle uk-grid-collapse" uk-grid>
        <div class="uk-width-1-3 uk-visible@s">



            <div class="uk-text-center">
                <div class="uk-inline-clip uk-light">
                    <img itemprop="image" class="img-anime-avatar" src="<?=$value->imageLow?>" alt="<?=$value->name?>">
                    <div class="uk-position-center">
                        <span class="uk-transition-fade" uk-icon="icon: play; ratio: 5"></span>
                    </div>
                <div class="uk-text-success uk-overlay uk-overlay-primary uk-position-bottom" style="padding: 5px; border-radius: 0px 0px 0px 3px;">
                    <div class="uk-text-bold" style="font-size: 18px;"><?=$value->realSeries?> из <?=$value->amountSeries?></div>
                    <div class="uk-text-small">эпизодов</div>
                </div>
                </div>
            </div>

            <div class="uk-position-top-left uk-overlay uk-overlay-default <?=$class?>"><?=$markerText?></div>

        </div>
        <div class="uk-width-expand@s">
            <div class="uk-card-body">

                <article class="uk-comment uk-hidden@s">
                    <header class="uk-comment-header uk-grid-medium uk-grid-collapse uk-flex-middle" uk-grid>
                        <div class="uk-width-auto">
                            <img class="img-anime-avatar" src="<?=$value->imageLow?>" width="80" alt="<?=$value->name?>">
                        </div>
                        <div class="uk-width-expand">
                                <ul class="uk-subnav uk-subnav-pill" style="display: inline-block;  margin: 0; margin-left: -10px; margin-bottom: 8px;">
                                    <li><a href="/anime/<?=$value->urlName?>" title="Смотреть онлайн <?=$value->name?> в высоком качестве"><span><span uk-icon="icon: play;"></span>  <?=$value->name?></span></a></li>
                                </ul>

                                <ul class="uk-subnav uk-subnav-pill uk-text-right" style="display: inline-block;  margin: 0; margin-left: -10px;">
                                    <li><a href="/anime/<?=$value->urlName?>" style="color: #3399ff!important;" title="Смотреть высоком качестве <?=$value->name?>"><?=$value->realSeries?> из <?=$value->amountSeries?> эпизодов</a></li>
                                </ul>

                            </ul>
                        </div>
                    </header>
                </article>


                <ul class="uk-visible@s uk-subnav uk-subnav-pill" style="display: inline-block;  margin: 0; margin-left: -20px; margin-bottom: 8px;">
                    <li><a href="/anime/<?=$value->urlName?>" title="Смотреть онлайн <?=$value->name?> в высоком качестве"><span itemprop="name"><span uk-icon="icon: play;"></span>  <?=$value->name?></span></a></li>
                </ul>

                <div><div class="anime-date uk-text-small">

                    <meta itemprop="datePublished" content="<?=date ("Y-m-d", $value->time)?>">

                    <span uk-icon="icon: calendar"></span> <?=$value->year?>,
                    <?php if ($value->season == 1): ?>
                        Зимний
                    <?php elseif ($value->season == 2):?>
                        Весенний
                    <?php elseif ($value->season == 3):?>
                        Летний
                    <?php else:?>
                        Осенний
                    <?php endif ?>
                        сезон
                </div></div>

                <div class="uk-text-small">
                <?php foreach ($value->genre as $key2 => $value2): ?>

                    <a class="anime-list-link" href="/Search?name=&send=&sort=1&genres[]=<?=$value2['id']?>" title="Аниме <?=$value2['name']?>"><span uk-icon="icon: tag; ratio: 0.8"></span> <span itemprop="genre"><?=$value2['name']?></span></a>

                <?php endforeach ?>
                </div>

                <div class="uk-text-small">
                <?php foreach ($value->voice as $key2 => $value2): ?>

                    <a class="anime-list-link anime-list-link-dub" href="/Search?name=&send=&sort=1&voices[]=<?=$value2['id']?>" title="Озвучка <?=$value2['name']?>"><span uk-icon="icon: play; ratio: 0.8"></span> <?=$value2['name']?></a>

                <?php endforeach ?>
                </div>

                <div class="anime-date uk-text-small">

                    <span itemprop="director" itemscope itemtype="http://schema.org/Person">

                        <span itemprop="name"><span uk-icon="icon: bolt; ratio: 0.8"></span> <?=((empty($value->producer)) ? 'неизвестно' : $value->producer)?></span></span>

                </div>

            </div>

        </div>
    </div>

</div>









<div class="uk-card uk-card-default uk-card-body box-shadow-none" style="padding-bottom: 0;"><div>
                    
<script type="text/javascript">

    function markerSerie (id) {

        $.ajax({
        type: "GET",
        url: 'http://anime-base.ru/Api/serieMarker/id/'+id,
        success: function (html) {
        $("#viewed"+id).html('<div class="uk-text-small uk-text-success"><span uk-icon="icon: phone"></span> Просмотрено</div>');
        }
        });

    }

    function getPlayer(id) {


        $.ajax({
        type: "GET",
        url: 'http://anime-base.ru/Api/seriePlayer/id/'+id,
        success: function (html) {
        $("#serie-player"+id).html(html);

        }
        });

        markerSerie(id);
    }

    setInterval(function(){
        $("#player_container").remove();
    }, 100); 


</script>

<ul uk-switcher="active: 1" uk-tab class="uk-width-1-1" style="margin-top: 0px;">
    <li><a href="#description" title="<?=$anime->name?> описание">Описание</a></li>
    <li><a href="#seriesList" class="uk-text-primary" title="<?=$anime->name?> список серий">Серии <span class="uk-label uk-label-primary uk-text-small"  style="margin-left: 10px; font-size: .775rem;"><?=$anime->realSeries?></span></a></li>

    <li><a href="#recall" title="<?=$anime->name?> отзывы" class="uk-text-warning">Мнения <span class="uk-label uk-label-warning uk-text-small"  style="margin-left: 10px; font-size: .775rem;"><?=$anime->recallTotal?></span></a></li>

</ul>

<ul class="uk-switcher uk-margin uk-width-1-1" tyle=" width: 100%;">
    <li><div itemprop="description" class="uk-text-primary" style="font-size: 12px;"><?=$anime->description?></div>

    <span class="uk-text-muted uk-text-small">Просмотров <span style="font-size: .735rem; background: #f2f2f2; color: #3e3e3e;" class="uk-label"><?=$anime->views?></span></span>
</li>
    <li>

<div class="uk-grid-collapse" uk-grid style="background: #ffffff;">

    <?php foreach ($series as $key => $value):?>
<div class="uk-width-1-2@m uk-width-1-1@s">

<div>
<a onclick="getPlayer (<?=$value->id?>);" href="#my-video<?=$value->id?>" title="<?=$anime->name?> <?=$value->title?> " class="serie-link"  uk-toggle>
<div uk-grid>
    <div class="uk-width-auto">
        <div class="photo" style="width: 50px; height: 50px; background-image: url(<?=(empty ($value->photo)? $anime->imageSmall : $value->photo)?>"></div>
    </div>
    <div class="uk-width-expand">

        <span uk-icon="icon: play"></span> <?=$value->title?>

        <div id="viewed<?=$value->id?>"><div class="uk-text-small uk-text-<?=($value->viewed > 0 ? 'success' : 'muted')?>">
            <span uk-icon="icon: phone"></span> <?=($value->viewed > 0 ? 'Просмотрено' : 'Не просмотрено')?>
        </div></div>

    </div>
</div>
</a>
</div>

<div id="my-video<?=$value->id?>" onclick="document.getElementById('film_main<?=$value->id?>').src = '/d/404';" uk-modal>

    <div class="uk-modal-dialog uk-modal-body" style="padding: 0px; height: 90% !important; width: 100% !important; background: #000000;">

        <!-- wqe <?php $this->load->view ("advertising/advertisingRand", ['serieId' => $value->id]) ?> -->

        <a href="" onclick="document.getElementById('film_main<?=$value->id?>').src = '/d/404'; document.getElementById('film_main<?=$value->id?>').html('');" class="uk-modal-close uk-close uk-close-alt"></a>
        <div id="serie-player<?=$value->id?>" style="height: 100%;"></div>
        <div class="place"><div class="container"></div></div>

    </div>
</div>

</div>
    <?php endforeach ?>
</div>


    </li>

    <li>

<div class="uk-grid-collapse" uk-grid style="background: #ffffff;">

<?php foreach ($anime->recall as $key => $value): ?>

<div class="uk-width-1-1">

<div>
<a class="serie-link">
<div class="uk-grid-collapse" uk-grid>
    <div class="uk-width-auto">
    <img class="uk-border-circle" style="height: 75px; margin-right: 15px;" src="<?=$value->user->avatar?>" alt="<?=$anime->name?> обсуждения">

    </div>
    <div class="uk-width-expand">

        <span uk-icon="icon: user"></span> <?=$value->user->firstName?> <?=$value->user->lastName?>

        <span class="uk-float-right" style="font-size: 11px;"><span uk-icon="icon: clock"></span> <?=$value->time?></span>

        <div><div class="uk-text-small uk-text-muted" style="font-size: 13px; text-transform: none !important;">
            <span uk-icon="icon: comments; ratio: 0.8" style="margin-right: 5px;"></span> <?=$value->message?> 
        </div></div>

    </div>
</div>
</a>
</div>

</div>

<?php endforeach ?>

</div>
<?php if (empty($this->user->avatar)): ?>

    <div class="uk-alert-danger" uk-alert>
    <a class="uk-alert-close" title="Аниме <?=$anime->name?> отзывы" uk-close></a>
    <p>Чтобы оставить мнение, вам нужно авторизоваться через Вконтакте!</p>
    </div>

<?php else: ?>

<form action="" method="POST">
    <fieldset class="uk-fieldset">
            <textarea class="uk-textarea" name="message" rows="4" placeholder="Напишите ваше мнение..."></textarea>
        <div class="uk-margin" style="margin-top: 0 !important;">
            <button class="uk-width-1-1  uk-button uk-button-primary">Добавить</button>
        </div>

    </fieldset>
</form>

<?php endif ?>

    </li>

</ul>

    </div>
</div>
<div class="uk-card uk-card-default box-shadow-none uk-card-body uk-margin" style="    margin-bottom: 0;
    margin-top: 0 !important;
    padding: 20px 20px 0px 30px;
    border-top: 1px solid #eee;">
<?php if (isset($anime->seasons[1])): ?>

<div style="    background: #32d296;
    display: inline-block;
    padding: 5px;
    border-radius: 2px;
    color: #fff; font-weight: 400;">СЕЗОНЫ</div>

<div class="uk-grid-collapse" uk-grid style="background: #ffffff;">

<?php foreach ($anime->seasons as $key => $season): ?>

<div class="uk-width-1-1">

<div>
<a href="/anime/<?=$season->urlName?>/" title="Смотреть онлайн <?=$season->name?>" class="serie-link">
<div class="uk-grid-collapse" uk-grid>
    <div class="uk-width-auto">
        <div class="photo" style="width: 90px; height: 50px; margin-right: 10px;background-image: url(<?=$season->imageSmall?>)"></div>
    </div>
    <div class="uk-width-expand">

        <span uk-icon="icon: play"></span> <?=$season->name?>

        <div><div class="uk-text-small uk-text-muted" style="font-size: 13px; text-transform: none !important;">
            <?=$season->year?> ГОД 
        </div></div>

    </div>
</div>
</a>
</div>

</div>

<?php endforeach ?>

<?php endif ?>

    </div>

<?=($checkInList < 1 ? '<a class="uk-button uk-width-1-1 uk-button-default uk-text-success" title="Сохранить аниме" href="/anime/'.$anime->urlName.'/addToList" style="background: #ffffff; padding: 7px;"><span uk-icon="icon: future"></span> Отпложить просмотр</a>' : ' 

<div class="uk-button-group uk-width-1-1" style="background: #ffffff; padding: 0px;">
    <button class="uk-width-3-4  uk-button" style="background: #32d296;
    color: #ffffff;" disabled><span uk-icon="icon: future"></span> Добавлено</button>

        <a href="#" title="'.$anime->name.' добавить" class="uk-button uk-button-danger uk-width-1-4"><span uk-icon="icon:  close"></span></a>

</div>

')?>
