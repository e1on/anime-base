
<?php
if (isset($error)) echo var_dump($error);
?>

    <div class="place">

        <div class="container">
        <ul class="uk-breadcrumb">
    <li><a href="/Admin">Админка</a></li>
    <li class="uk-active"><span>Добавить аниме</span></li>
</ul>
        </div>

    </div>

    <div class="place">

        <div class="container">

        	<form action="" enctype="multipart/form-data" class="uk-form" method="POST">
        		<p>
        			Название: <br />
        			<input type="text" name="name">
        		</p>

        		<p>
        			Год выхода: <br />
        			<input type="text" size="4" name="year" value="<?=date("Y")?>">

        			<select name="season">
					    <option selected value="1">Зимний сезон</option>
					    <option value="2">Весенний сезон</option>
					    <option value="3">Летний сезон</option>
					    <option value="4">Осенний сезон</option>
   					</select>
   				</p>

        		<p>
        			Количество серий: <br />
        			<input type="number" size="4" id="form-s-mix2" min="1" max="800" value="12" class="uk-form-width-mini uk-form-small" name="amountSeries" style="width: 50px;">
        		</p>

        		<p>

<div class="uk-button-dropdown" data-uk-dropdown="" aria-haspopup="true" aria-expanded="false">
                                <button class="uk-button">Жанры <i class="uk-icon-caret-down"></i></button>
                                <div class="uk-dropdown uk-dropdown-scrollable uk-dropdown-bottom" style="top: 30px; left: 0px;">

                                <?php foreach ($genres as $key => $value): ?>
                                    <label><input type="checkbox" name="genre[]" value="<?=$value->id?>"> <?=$value->name?></label><br />
                                <?php endforeach ?>

                                </div>
                            </div>


<div class="uk-button-dropdown" data-uk-dropdown="" aria-haspopup="true" aria-expanded="false">
                                <button class="uk-button">Озвучивание <i class="uk-icon-caret-down"></i></button>
                                <div class="uk-dropdown uk-dropdown-scrollable uk-dropdown-bottom" style="top: 30px; left: 0px;">

                                <?php foreach ($voices as $key => $value): ?>
                                    <label><input type="checkbox" name="voice[]" value="<?=$value->id?>"> <?=$value->name?></label><br />
                                <?php endforeach ?>

                                </div>
                            </div>


        		</p>

                <p>
                    Режиссёр: <br />
                    <input type="text" name="producer">
                </p>

        		<p>
        			Описание: <br />
        			<textarea name="description"></textarea>
        		</p>

        		<p>
   					<input type="file" name="image">
        		</p>


        		<button class="uk-button" name="send">Добавить</button>

        	</form>

        </div>

    </div>