<?php

/**
 * @package     Deelfy core
 * @link        http://deelfy.com
 * @copyright   Copyright (C) 2013 Deelfy Community
 * @author      Alexey Shpilka
 */

// ~~~~~~~~~~~~~~~~~~~~Ядро для обработки изображений~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

class Image {
 
// Глобальные переменные 
 
    var $image;
    var $image_type;
 
// Загрузка изображения
 
function load($filename) {

    $image_info = getimagesize($filename);
    $this->image_type = $image_info[2];
    if ( $this->image_type == IMAGETYPE_JPEG ) {
    $this->image = imagecreatefromjpeg($filename);
    } else if ( $this->image_type == IMAGETYPE_GIF ) {
    $this->image = imagecreatefromgif($filename);
    } else if ( $this->image_type == IMAGETYPE_PNG ) {
    $this->image = imagecreatefrompng($filename);
    }
	
}

function img_resize ($src, $dest, $max_width, $max_height, $rgb=0xFFFFFF, $quality=40) {

 if (!file_exists($src)) return false;
 $size = getimagesize($src);
 if ($size === false) return false;
 $src_width=$size[0]; $src_height=$size[1]; $src_left = $src_top = 0;
 $src_copy_width = $src_width;  $src_copy_height = $src_height;
 $dw=$max_width/$src_width; $dh=$max_height/$src_height;
 if ($dw<$dh) { //обрез.по ширине
  $src_copy_width=round($src_height*$max_width/$max_height);
  $src_left=round(($src_width-$src_copy_width)/2); 
 }
 else { //обрез.по высоте 
  $src_copy_height=round($src_width*$max_height/$max_width);
  $src_top=round(($src_height-$src_copy_height)/2); 
 }
 $format = strtolower(substr($size['mime'], strpos($size['mime'], '/')+1));
 $icfunc = 'imagecreatefrom'.$format;
 if (!function_exists($icfunc)) return false;
 $isrc = $icfunc($src);
 $idest = imagecreatetruecolor($max_width, $max_height);
 imagefill($idest, 0, 0, $rgb);
 imagecopyresampled($idest, $isrc, 0, 0, $src_left, $src_top, $max_width, $max_height, $src_copy_width, $src_copy_height);
 imagejpeg($idest, $dest, $quality);
 imagedestroy($isrc);
 imagedestroy($idest);
 return true;
 
}
// Сохранение изображения

function save($filename, $image_type=IMAGETYPE_JPEG, $compression=90, $permissions=null) {

    if ( $image_type == IMAGETYPE_JPEG ) {
    imagejpeg($this->image,$filename,$compression);
    } else if ( $image_type == IMAGETYPE_GIF ) {
    imagegif($this->image,$filename);
    } else if ( $image_type == IMAGETYPE_PNG ) {
    imagepng($this->image,$filename);
    }
    if ($permissions != null) {
    chmod($filename,$permissions);
    }

    $this->image = null;
    $this->image_type = null;
    
}

// Ширина изображения

function getWidth() {

    return imagesx($this->image);
	
}

// Высота изображения

function getHeight() {
	
    return imagesy($this->image);
	
}
 
// Конвертируем

function resize($width,$height) {

    imagesavealpha($this->image, true);
    $new_image = imagecreatetruecolor($width, $height);
    imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
    $this->image = $new_image;
	
}      
     
}

?>