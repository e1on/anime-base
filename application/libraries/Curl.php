<?php

class Curl
{
    public $description;
    
    public function __construct ()
    {
        if (!function_exists('curl_init'))
        {
            throw new \Exception('Library cURL not found');
        }
        
        $this->description = curl_init();
    }
    
    public function setUrl ($url)
    {
        curl_setopt($this->description, CURLOPT_URL, $url);
        return $this;
    }
    
    public function setOption ($option, $value = '')
    {
        if (is_array($option))
        {
            foreach ($option as $opt => $value)
            {
                curl_setopt($this->description, $opt, $value);
            }
        }
        else
        {
            curl_setopt($this->description, $option, $value);
        }
        
        return $this;
    }
    
    public function setUa ($ua)
    {
        curl_setopt($this->description, CURLOPT_USERAGENT, $ua);
        return $this;
    }

    public function setHeader ($array)
    {
        curl_setopt($this->description,CURLOPT_HTTPHEADER,$array);
        return $this;
    }

    public function setCookie ($cookieString)
    {
        curl_setopt($this->description, CURLOPT_COOKIE, $cookieString);
        return $this;
    } 

    public function useProxy ($ip) {
        curl_setopt ($this->description, CURLOPT_PROXY, $ip); 
        curl_setopt ($this->description, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5); 
        return $this;
    }

    public function getQuery ()
    {
        curl_setopt($this->description, CURLOPT_RETURNTRANSFER, TRUE);
        $results = curl_exec($this->description);

        

        if ($results === false) {
            echo "Proxy is not working: ", curl_error($this->description);
            exit;
        }
        else {
            return $results;
        }
    }
    
    public function setPost ($data)
    {
        if (!is_array($data))
        {
            throw new \Exception('curl::setPost() аргумент должен быть массивом');
        }
        
        curl_setopt($this->description, CURLOPT_POST, TRUE);
        curl_setopt($this->description, CURLOPT_POSTFIELDS, $data);
        
        return $this;
    }
}