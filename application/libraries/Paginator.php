<?php

class Paginator
{
	private $_perPage;
	private $_instance;
	public $_page;
	private $_limit;
	private $_totalRows = 0;

	public function initialize ($perPage, $instance)
    {
		$this->_instance = $instance;		
		$this->_perPage = $perPage;
		$this->set_instance();		
	}
     
	public function get_start()
    {
        return ($this->_page * $this->_perPage) - $this->_perPage;
	}
    
	private function set_instance()
    {
        $this->_page = (int) (!isset($_GET[$this->_instance]) ? 1 : $_GET[$this->_instance]); 
        $this->_page = ($this->_page == 0 ? 1 : $this->_page);
	}
    
	public function set_total($_totalRows)
    {
		$this->_totalRows = $_totalRows;
	}
    
	public function get_limit()
    {
        return "LIMIT ".$this->get_start().",".$this->_perPage;
    }
    
	public function page_links($path = '?', $ext = NULL)
	{
	    $adjacents = "2";
	    $prev      = $this->_page - 1;
	    $next      = $this->_page + 1;
	    $lastpage  = ceil($this->_totalRows/$this->_perPage);
	    $lpm1      = $lastpage - 1;

	    $pagination = "";
        
		if($lastpage > 1)
		{   
		    $pagination .= '<div class="uk-card uk-card-default uk-card-body box-shadow-none"><ul class="uk-pagination uk-flex-center">';
            
            if ($this->_page > 1)
            {
                $pagination.= '<li><a title="Назад" href="'.$path.$this->_instance.'='.$prev.$ext.'"><span class="uk-margin-small-right" uk-pagination-previous></span> Назад</a></a></li>';
            }
            else
            {
                $pagination.= '<li><span class="disabled"><span class="uk-margin-small-right" uk-pagination-previous></span> Назад</a></span></li>';
            }
            
            if ($lastpage < 7 + ($adjacents * 2))
            {   
                for ($counter = 1; $counter <= $lastpage; $counter++)
                {
                    if ($counter == $this->_page)
                    {
                        $pagination.= '<li class="uk-active"><span>'.$counter.'</span></li>';
                    }
                    else
                    {
                        $pagination.= '<li><a title="Страница '.$counter.'" href="'.$path.$this->_instance.'='.$counter.$ext.'">'.$counter.'</a></li>';
                    }
                }
            }
            elseif($lastpage > 5 + ($adjacents * 2))
            {
                if($this->_page < 1 + ($adjacents * 2))       
                {
                    for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
                    {
                        if ($counter == $this->_page)
                        {
                          $pagination.= '<li><span class="current">'.$counter.'</span></li>';
                        }
                        else
                        {
                            $pagination.= '<li><a title="Страница '.$counter.'" href="'.$path.$this->_instance.'='.$counter.$ext.'">'.$counter.'</a></li>';
                        }
                    }
                  
                    $pagination.= '...';
                    $pagination.= '<li><a title="Страница '.$lpm1.'" href="'.$path.$this->_instance.'='.$lpm1.$ext.'">'.$lpm1.'</a></li>';
                    $pagination.= '<li><a title="Страница '.$lastpage.'" href="'.$path.$this->_instance.'='.$lastpage.$ext.'">'.$lastpage.'</a></li>';       
                }
                elseif($lastpage - ($adjacents * 2) > $this->_page && $this->_page > ($adjacents * 2))
                {
                    $pagination.= '<li><a title="Страница 1" href="'.$path.$this->_instance.'=1'.$ext.'">1</a></li>';
                    $pagination.= '<li><a title="Страница 2" href="'.$path.$this->_instance.'=2'.$ext.'">2</a></li>';
                    $pagination.= '...';
                  
                    for ($counter = $this->_page - $adjacents; $counter <= $this->_page + $adjacents; $counter++)
                    {
                        if ($counter == $this->_page)
                        {
                            $pagination.= '<span class="current">'.$counter.'</span>';
                        }
                        else
                        {
                            $pagination.= '<li><a title="Страница '.$counter.'" href="'.$path.$this->_instance.'='.$counter.$ext.'">'.$counter.'</a></li>';
                        }
                    }
                  
                    $pagination.= '..';
                    $pagination.= '<li><a title="Страница '.$lpm1.$ext.'" href="'.$path.$this->_instance.'='.$lpm1.$ext.'">'.$lpm1.'</a></li>';
                    $pagination.= '<li><a title="Страница '.$lastpage.'" href="'.$path.$this->_instance.'='.$lastpage.$ext.'">'.$lastpage.'</a></li>';       
                }
                else
                {
                    $pagination.= '<li><a title="Страница 1" href="'.$path.$this->_instance.'=1'.$ext.'"">1</a></li>';
                    $pagination.= '<li><a title="Страница 2" href="'.$path.$this->_instance.'=2'.$ext.'"">2</a></li>';
                    $pagination.= "..";
                
                    for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
                    {
                        if ($counter == $this->_page)
                        {
                            $pagination.= '<span class="current">'.$counter.'</span>';
                        }
                        else
                        {
                            $pagination.= '<li><a title="Страница '.$counter.'" href="'.$path.$this->_instance.'='.$counter.$ext.'">'.$counter.'</a></li>';    
                        }
                    }
                }
            }

            if ($this->_page < $counter - 1)
            {
                $pagination.= '<li><a title="Вперёд" href="'.$path.$this->_instance.'='.$next.$ext.'">Вперёд <span class="uk-margin-small-left" uk-pagination-next></span></a></li></ul></div>';
            }
            else
            {
                $pagination.= '<li><span class="disabled">Вперёд <span class="uk-margin-small-left" uk-pagination-next></span></span></li></ul>';
                $pagination.= '</ul></div>';
            }
        }

        return $pagination;
    }
}
