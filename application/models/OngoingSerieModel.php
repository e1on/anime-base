<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class OngoingSerieModel extends CI_Model {

	public function getLimit ($limit = 6) {



		$date = date("n")+2;
		if ($date == 13) $date = 1;

		if ($date == 12 or $date == 1 or $date == 2) $season = 1;
		if ($date == 3 or $date == 4 or $date == 5) $season = 2;
		if ($date == 6 or $date == 7 or $date == 8) $season = 3;
		if ($date == 9 or $date == 10 or $date == 11) $season = 4;

		$time = time()-604800;

		$this->load->model ("AnimeModel");
		$this->load->model ("TimeModel");

		$query = $this->db->query ("
			SELECT DISTINCT s.animeId, s.id, s.timeAdd, s.animeId, s.title, a.id 
			FROM animeSeries s 
			INNER JOIN anime a ON s.animeId = a.id 
			AND s.timeAdd > '".$time."' AND a.year = '".date("Y")."' AND a.season >= '".($season-1)."' GROUP BY a.id, s.timeAdd, s.animeId, s.title, a.id ORDER BY s.id DESC LIMIT 6");

		$result = $query->result ();
		$data = [];

		foreach ($result as $key => $value) {
			$value->anime = $this->AnimeModel->getBaseInfo ($value->animeId);
			$value->timeAdd = $this->TimeModel->translate ($value->timeAdd);
			$data[$key] = $value;
		}

		return $data;

	}

}