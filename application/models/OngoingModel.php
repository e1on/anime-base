<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class OngoingModel extends CI_Model {

	public function top () {

		$date = date("n")+2;
		if ($date == 13) $date = 1;

		if ($date == 12 or $date == 1 or $date == 2) $season = 1;
		if ($date == 3 or $date == 4 or $date == 5) $season = 2;
		if ($date == 6 or $date == 7 or $date == 8) $season = 3;
		if ($date == 9 or $date == 10 or $date == 11) $season = 4;

		$query = $this->db->query ("SELECT * FROM `anime` WHERE `year` = '".date("Y")."' AND `season` >= '".($season-1)."' ORDER BY `views` DESC LIMIT 6");
		$result = $query->result ();

		return $result;

	}

	public function last () {

		$date = date("n")+2;
		if ($date == 13) $date = 1;

		if ($date == 12 or $date == 1 or $date == 2) $season = 1;
		if ($date == 3 or $date == 4 or $date == 5) $season = 2;
		if ($date == 6 or $date == 7 or $date == 8) $season = 3;
		if ($date == 9 or $date == 10 or $date == 11) $season = 4;

		$query = $this->db->query ("SELECT * FROM `anime` WHERE `year` = '".date("Y")."' AND `season` >= '".($season-1)."' ORDER BY `id` DESC LIMIT 4");
		$result = $query->result ();

		return $result;

	}

}