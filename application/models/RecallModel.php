<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RecallModel extends CI_Model {

	public function getByAnimeIdLimit ($animeId) {

		$query = $this->db->query ("SELECT * FROM `recall` WHERE `animeId` = ? ORDER BY `id` DESC LIMIT 10 ", [$animeId]);

		$result = $query->result ();
		$data = [];

		$this->load->model ("User/AccessModel");
		$this->load->model ("TimeModel");

		foreach ($result as $key => $value) {
			$value->user = $this->AccessModel->getById ($value->userId);
			$value->time = $this->TimeModel->translate ($value->time);
			$data[$key] = $value;
		}

		return $data;

	}

	public function getTotal ($animeId) {

		$query = $this->db->query ("SELECT `id` FROM `recall` WHERE `animeId` = ?", [$animeId]);

		return $query->num_rows ();

	}

	public function add ($animeId, $userId, $message) {
		
		$this->db->query ("INSERT INTO `recall` SET `animeId` = ?, `userId` = ?, `message` = ?, `time` = '".time ()."'", [$animeId, $userId, $message]);

		return;

	}

}