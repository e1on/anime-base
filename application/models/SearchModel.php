<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SearchModel extends CI_Model {

	public function search ($data = []) {

		if (isset($data['sql']) and $data['limit']) {

			$queryAnime = $this->db->query ($data['sql']." ".$data['limit']);

			if ($queryAnime->num_rows () > 0) {

				$rows = $queryAnime->result ();
				$data = [];

				$this->load->model ("AnimeModel");
				foreach ($rows as $key => $value) {

					$anime = $this->AnimeModel->handler ($value);
					$data[$key] = $anime;

				}

				return $data;

			}

			return $queryAnime->num_rows ();
		}

	}

    public function translate ($time) {

        switch (date('j n Y', $time)) { 
            
            default: 
                return date('Y-m-j', $time);
                break; 

            } 

    }

}