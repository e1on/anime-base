<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AnimeSerieModel extends CI_Model {

	public function addSeries ($animeId, $series) {

		foreach ($series as $key => $value) {

			$this->db->query ("INSERT INTO `animeSeries` SET `animeId` = ?, `title` = ?, `description` = ?, `player` = ?, `photo320` = ?", [$animeId, $value['title'], $value['description'], $value['player'],$value['photo_320']]);

		}

		$this->db->query ("UPDATE `anime` SET `timeAdd` = '".time ()."' WHERE `id` = '".$animeId."'");
	}

	public function addSerie ($animeId, $serie, $name) {

			$this->db->query ("INSERT INTO `animeSeries` SET `animeId` = ?, `title` = ?, `player` = ?, `timeAdd` = '".time ()."'", [$animeId, $name, $serie]);
			$this->db->query ("UPDATE `anime` SET `timeAdd` = '".time ()."' WHERE `id` = '".$animeId."'");
	}

	public function getByAnimeId ($id) {

		$anime = $this->db->query ("SELECT * FROM `anime` WHERE `id` = ?", [$id])->row()->amountSeries;

		$result = $this->db->query ("SELECT * FROM `animeSeries` WHERE `animeId` = ? LIMIT 0, ".$anime, [$id])->result ();
		$result = $this->handlerName ($result);
		return $result;

	}

	public function check ($id) {

		$result = $this->db->query ("SELECT * FROM `animeSeries` WHERE `id` = ?", [$id])->num_rows ();
		return $result;

	}	

	public function get ($id) {

		$result = $this->db->query ("SELECT * FROM `animeSeries` WHERE `id` = ?", [$id])->row ();

		$findme   = '|';
		$pos = strpos($result->player, $findme);

		if ($pos === false) {
		    return $result;
		}


		$str = strpos($result->player, "|");
		$result->player = substr($result->player, 0, $str);
		return $result;

	}

	public function handlerName ($serieData = []) {

		$data = [];

		foreach ($serieData as $key => $value) {

			preg_match ('|([0-9]) (серия)|is', $value->title, $result);
			preg_match ('|([0-9][0-9]) (серия)|is', $value->title, $result2);

			preg_match ('|([0-9]) сезон ([0-9]) (серия)|is', $value->title, $result3);
			preg_match ('|([0-9]) сезон ([0-9][0-9]) (серия)|is', $value->title, $result4);

			$value->serieTitle = $value->title;

			if (!empty($result[0]) and isset($result[0])) $value->title = $result[0];
			if (!empty($result2[0]) and isset($result2[0])) $value->title = $result2[0];

			if (!empty($result3[0]) and isset($result3[0])) $value->title = $result3[0];
			if (!empty($result4[0]) and isset($result4[0])) $value->title = $result4[0];

			$value->viewed = $this->db->query ("SELECT `id` FROM `serieMarker` WHERE `serieId` = '".$value->id."' AND `userId` = '".$this->user->id."'")->num_rows ();

			$str = strpos($value->player, "|");
			$value->player = substr($value->player, 0, $str);

			$data[] = $value;

		}

		return $data;

	}

	public function serieMarker ($serieId, $userId) {
		
		$queryCheck = $this->db->query ("SELECT `id` FROM `serieMarker` WHERE `serieId` = ? AND `userId` = ?", [$serieId, $userId]);

		if ($queryCheck->num_rows () < 1) {

			$this->db->query ("INSERT INTO `serieMarker` SET `serieId` = ?, `userId` = ?, `runTime` = '".time ()."'", [$serieId, $userId]);
		}

		return;

	}

}