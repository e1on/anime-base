<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class VoiceModel extends CI_Model {

	public function getAll () {

		$query = $this->db->query ("SELECT * FROM `voices`");
		$data = $query->result ();

		return $data;

	}

	public function getById ($id) {

		$query = $this->db->query ("SELECT * FROM `voices` WHERE `id` = ?", [$id]);
		$data = $query->result ();

		return $data[0];

	}

}