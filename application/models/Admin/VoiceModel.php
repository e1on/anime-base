<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class VoiceModel extends CI_Model {

	public function add ($name) {

		$this->db->query ("INSERT INTO `voices` SET `name` = ?", [$name]);

		$voiceId = $this->db->query ("SELECT `id` FROM `voices` ORDER BY `id` DESC LIMIT 1")->row ()->id;

		return $voiceId;
	}

}