<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AnimeModel extends CI_Model {

	private function rus2translit($string) {
    $converter = array(
        'а' => 'a',   'б' => 'b',   'в' => 'v',
        'г' => 'g',   'д' => 'd',   'е' => 'e',
        'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
        'и' => 'i',   'й' => 'y',   'к' => 'k',
        'л' => 'l',   'м' => 'm',   'н' => 'n',
        'о' => 'o',   'п' => 'p',   'р' => 'r',
        'с' => 's',   'т' => 't',   'у' => 'u',
        'ф' => 'f',   'х' => 'h',   'ц' => 'c',
        'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
        'ь' => '\'',  'ы' => 'y',   'ъ' => '\'',
        'э' => 'e',   'ю' => 'yu',  'я' => 'ya',
        
        'А' => 'A',   'Б' => 'B',   'В' => 'V',
        'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
        'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
        'И' => 'I',   'Й' => 'Y',   'К' => 'K',
        'Л' => 'L',   'М' => 'M',   'Н' => 'N',
        'О' => 'O',   'П' => 'P',   'Р' => 'R',
        'С' => 'S',   'Т' => 'T',   'У' => 'U',
        'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
        'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
        'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '\'',
        'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
    );
    return strtr($string, $converter);
}

private function str2url($str) {
    // переводим в транслит
    $str = $this->rus2translit($str);
    // в нижний регистр
    $str = strtolower($str);
    // заменям все ненужное нам на "-"
    $str = preg_replace('~[^-a-z0-9_]+~u', '-', $str);
    // удаляем начальные и конечные '-'
    $str = trim($str, "-");
    return $str;
}
	public function add ($name, $year, $season, $amountSeries, $genres, $voices, $description, $producer, $path) {

$urlName = $this->str2url ($name);

		$this->db->query ("INSERT INTO `anime` SET `name` = ?, `year` = ?, `season` = ?, `amountSeries` = ?, `genres` = ?, `voices` = ?, `description` = ?, `producer` = ?, `image` = ?, `urlName` = ?, `time` = '".time ()."', `timeAdd` = '".time ()."'", [$name, $year, $season, $amountSeries, ':'.$genres.':', ':'.$voices.':', $description, $producer, $path, $urlName]);
		
		$genreId = $this->db->query ("SELECT `id` FROM `anime` ORDER BY `id` DESC LIMIT 1")->row ()->id;

		return $genreId;

	}

	public function endAnime () {
		$query = $this->db->query ("SELECT * FROM `anime` ORDER BY `id` DESC LIMIT 1");
		
		$result = $query->result ()[0];
		$data = $this->handler ($result);

		return $data;
	}
	public function handler ($result) {

		if (is_array ($result) and isset($result[0])) $result = $result[0];

		$this->load->model ("GenreModel");
		$this->load->model ("VoiceModel");
		$this->load->model ("TimeModel");

		$genresAnime = explode(":", $result->genres);
		$voicesAnime = explode(":", $result->voices);
		$result->genre = [];
		$result->voice = [];
		$result->datePublished = $this->TimeModel->translate ($result->time);
		
		foreach ($genresAnime as $key2 => $value2) {
			if (!empty($value2)){
				$result->genre[] = ['id' => $value2, 'name' => $this->GenreModel->getById ($value2)->name];
			}
		}

		foreach ($voicesAnime as $key2 => $value2) {
			if (!empty($value2)){
				$result->voice[] = ['id' => $value2, 'name' => $this->VoiceModel->getById ($value2)->name];
			}
		}

		return $result;

	}
}