<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GenreModel extends CI_Model {

	public function add ($name, $description='') {

		$this->db->query ("INSERT INTO `genres` SET `name` = ?, `description` = ?", [$name, $description]);

		$genreId = $this->db->query ("SELECT `id` FROM `genres` ORDER BY `id` DESC LIMIT 1")->row ()->id;

		return $genreId;

	}

}