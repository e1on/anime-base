<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GenreModel extends CI_Model {

	public function getAll () {

		$query = $this->db->query ("SELECT * FROM `genres`");
		$data = $query->result ();

		return $data;

	}

	public function check ($id) {

		$query = $this->db->query ("SELECT * FROM `genres` WHERE `id` = ?", [$id]);
		$data = $query->num_rows ();

		return $data;

	}


	public function getAllhandler () {

		$query = $this->db->query ("SELECT * FROM `genres`");
		$result = $query->result ();

		$data = [];

		foreach ($result as $key => $value) {

			$anime = $this->handler ($value);

			$data[$key] = $anime;

		}

		return $data;

	}

	public function handler ($result) {

		if (is_array ($result) and isset($result[0])) $result = $result[0];

		$this->load->model ("AnimeModel");
		$result->animeTotal = $this->AnimeModel->getCountInGenre ($result->id);
		$result->image = $this->AnimeModel->getAvatarGenre ($result->id);
		return $result;

	}

	public function getById ($id) {

		$query = $this->db->query ("SELECT * FROM `genres` WHERE `id` = ?", [$id]);
		$data = $query->result ();

		return $data[0];

	}

}