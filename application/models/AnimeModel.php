<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AnimeModel extends CI_Model {

	public function getAll ($limit = '') {

		$query = $this->db->query ("
			SELECT a.* 
			FROM anime a 
			INNER JOIN animeView v 
			ON a.id = v.animeId ORDER BY v.time DESC ".$limit);

		$result = $query->result ();

		$data = [];

		foreach ($result as $key => $value) {

			$anime = $this->handler ($value);

			$data[$key] = $anime;

		}

		return $data;
		
	}

	public function getOngoings ($limit = '') {

		$date = date("n")+2;
		if ($date == 13) $date = 1;

		if ($date == 12 or $date == 1 or $date == 2) $season = 1;
		if ($date == 3 or $date == 4 or $date == 5) $season = 2;
		if ($date == 6 or $date == 7 or $date == 8) $season = 3;
		if ($date == 9 or $date == 10 or $date == 11) $season = 4;

		$query = $this->db->query ("
			SELECT * FROM `anime` WHERE `year`='".date ('Y')."' AND `season` >= '".($season-1)."' ORDER BY `timeAdd` DESC ".$limit);
		$result = $query->result ();

		$data = [];

		foreach ($result as $key => $value) {

			$anime = $this->handler ($value);

			$data[$key] = $anime;

		}

		return $data;
		
	}

	public function getAllByGanre ($ganreId, $sort = '') {

		$query = $this->db->query ("SELECT * FROM `anime` WHERE `genres` LIKE '%:".$ganreId.":%' ".$sort." ");
		$result = $query->result ();

		$data = [];

		foreach ($result as $key => $value) {

			$anime = $this->handler ($value);

			$data[$key] = $anime;

		}

		return $data;
		
	}

	public function getNew ($sort = '') {

		$query = $this->db->query ("SELECT * FROM `anime` ORDER BY `year` DESC ".$sort."");
		$result = $query->result ();

		$data = [];

		foreach ($result as $key => $value) {

			$anime = $this->handler ($value);

			$data[$key] = $anime;

		}

		return $data;
		
	}

	public function check ($id) {

		$res = $this->db->query ("SELECT * FROM `anime` WHERE `id` = ?", [$id])->num_rows ();
		return $res;

	}

	public function checkName ($name) {

		$res = $this->db->query ("SELECT * FROM `anime` WHERE `urlName` = ?", [$name])->num_rows ();
		return $res;

	}

	public function get ($id) {

		$query = $this->db->query ("SELECT * FROM `anime` WHERE `id` = ?", [$id]);
		$result = $query->result ()[0];

		$data = $this->handler ($result);
		$data->seasons = $this->getSeasons ($data->name);

		$this->load->model ("RecallModel");

		$data->recall = $this->RecallModel->getByAnimeIdLimit ($data->id);
		$data->recallTotal = $this->RecallModel->getTotal ($data->id);

		return $data;

	}

	public function getByName ($name) {

		$query = $this->db->query ("SELECT * FROM `anime` WHERE `urlName` = ?", [$name]);
		$result = $query->result ()[0];

		$data = $this->handler ($result);
		$data->seasons = $this->getSeasons ($data->name);

		$this->load->model ("RecallModel");

		$data->recall = $this->RecallModel->getByAnimeIdLimit ($data->id);
		$data->recallTotal = $this->RecallModel->getTotal ($data->id);

		return $data;

	}

	public function getBaseInfo ($id) {

		$query = $this->db->query ("SELECT `id`, `name`, `views`, `amountSeries`, `image`, `urlName` FROM `anime` WHERE `id` = ?", [$id]);
		$result = $query->result ()[0];

		return $result;

	}

	public function getSeasons ($name) {

		$str=strpos($name, "/");
		$name=substr($name, 0, $str);
		$name = str_replace("ТВ-1", "", $name);
		$name = str_replace("ТВ-2", "", $name);
		$name = str_replace("ТВ-3", "", $name);
		$name = str_replace("ТВ-4", "", $name);
		$name = str_replace("ТВ-5", "", $name);
		$name = str_replace("ТВ-6", "", $name);
		$name = str_replace("ТВ-7", "", $name);
		$name = str_replace("TV-1", "", $name);
		$name = str_replace("TV-2", "", $name);
		$name = str_replace("TV-3", "", $name);
		$name = str_replace("TV-4", "", $name);
		$name = str_replace("TV-5", "", $name);
		$name = str_replace("TV-6", "", $name);
		$name = str_replace("TV-7", "", $name);
		$name = str_replace("OVA-1", "", $name);
		$name = str_replace("OVA-2", "", $name);
		$name = str_replace("OVA-3", "", $name);
		$name = str_replace("OVA-4", "", $name);
		$name = str_replace("OVA", "", $name);
		$name = ltrim ($name);
		$name = trim ($name);

		$query = $this->db->query ("SELECT * FROM `anime` WHERE `name` LIKE '%".$this->db->escape_like_str($name)."%' ORDER BY `year` ASC");
		$result = $query->result ();

		return $result;

	}

	public function handler ($result) {

		if (is_array ($result) and isset($result[0])) $result = $result[0];

		$this->load->model ("GenreModel");
		$this->load->model ("VoiceModel");
		$this->load->model ("TimeModel");
		$this->load->model ("ViewModel");

		$genresAnime = explode(":", $result->genres);
		$voicesAnime = explode(":", $result->voices);
		$result->genre = [];
		$result->voice = [];
		$result->datePublished = $this->TimeModel->translate ($result->time);
		$result->realSeries = $this->db->query ("SELECT `id` FROM `animeSeries` WHERE `animeId` = '".$result->id."'")->num_rows ();

		foreach ($genresAnime as $key2 => $value2) {
			if (!empty($value2)){
				$result->genre[] = ['id' => $value2, 'name' => $this->GenreModel->getById ($value2)->name];
			}
		}

		foreach ($voicesAnime as $key2 => $value2) {
			if (!empty($value2)){
				$result->voice[] = ['id' => $value2, 'name' => $this->VoiceModel->getById ($value2)->name];
			}
		}

		return $result;

	}

	public function getCountInGenre ($genreId) {

		$query = $this->db->query ("SELECT * FROM `anime` WHERE `genres` LIKE '%:".$genreId.":%'");
		$result = $query->num_rows ();

		return $result;

	}

	public function getAvatarGenre ($genreId) {

		$query = $this->db->query ("SELECT `image` FROM `anime` WHERE `genres` LIKE '%:".$genreId.":%' LIMIT 1");
		$result = $query->result ();

		if (isset($result[0]->image)) return $result[0]->image;
		return '/images/no-image.png';

	}

}