<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AnimeUserListModel extends CI_Model {

	public function checkInList ($animeId, $userId) {

		$queryCheck = $this->db->query ("SELECT `id` FROM `animeUserList` WHERE `userId` = ? AND `animeId` = ?", [$userId, $animeId]);

		return $queryCheck->num_rows ();

	}

	public function setInList ($animeId, $userId) {

		$this->db->query ("INSERT INTO `animeUserList` SET `userId` = ?, `animeId` = ?, `addTime` = '".time ()."'", [$userId, $animeId]);

		return;

	}

	public function getUserList ($userId) {

		$queryList = $this->db->query ("SELECT * FROM `animeUserList` WHERE `userId` = ?", [$userId]);

		$result = $queryList->result ();

		$data = [];
		foreach ($result as $key => $value) {

			$anime = $this->handler ($value);

			$data[$key] = $anime;

		}

		return $data;

	}

	public function handler ($row) {
		
		$this->load->model ("AnimeModel");
		$row->anime = $this->AnimeModel-> get ($row->animeId);

		return $row;
	}

}