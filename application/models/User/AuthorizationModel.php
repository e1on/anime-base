<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AuthorizationModel extends CI_Model {

	public function assignPowers (stdClass $user) {

		$this->user->signin = true;

		foreach ($user as $key => $value) {
			$this->user->$key = $value;
		}

		$time = time()-250;
		if ($user->lastTime < $time) {
			$this->db->query ("UPDATE `users` SET `lastTime` = '".time()."' WHERE `id` = ?", [$user->id]);
		}

		if ($user->ip != $this->input->ip_address()) {
			$this->db->query ("UPDATE `users` SET `ip` = ? WHERE `id` = ?", [$this->input->ip_address(), $user->id]);
		}

		if ($user->ua != $this->input->ip_address()) {
			$this->db->query ("UPDATE `users` SET `ip` = ? WHERE `id` = ?", [$this->input->ip_address(), $user->id]);
		}


		return;
		
	}

}
