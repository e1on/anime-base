<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AuthenticationModel extends CI_Model {

	public function __construct () {

		$vk = $this->input->cookie ('sessionId');
		$save = $this->input->cookie ('saveId');

		$ip = $this->input->ip_address();

		$this->load->model ("User/AuthorizationModel");

		if (empty($vk) or $vk === NULL) {

			$queryRes = $this->db->query("SELECT * FROM `users` WHERE `ip` = ? OR `saveId` = ?  LIMIT 1", [$ip, $save]);

			if ($queryRes->num_rows () > 0) {

				$row = $queryRes->row ();

				if (!empty($row->vkAccessToken)) {

					setcookie('sessionId', $row->vkAccessToken, time()+86400*24, '/', '.anime-base.ru');

					if (isset($_SERVER['PATH_INFO'])) $p = $_SERVER['PATH_INFO'];
					else $p = '/';

					header("Location: ".$p);
					exit;

				}
				$this->AuthorizationModel -> assignPowers ($row);
				return;

			}

			$this->signup ($ip);
			return;

		}

		$query = $this->db->query ("SELECT * FROM `users` WHERE `vkAccessToken` = ? LIMIT 1", [$vk]);

		if ($query->num_rows () > 0) {
			$this->AuthorizationModel -> assignPowers ($query->row ());
			return;
		}

		setcookie('sessionId', '', -1, '/', '.anime-base.ru');

	}

	private function signup ($ip) {

			$this->load->model ("User/SignupModel");
			$user = $this->SignupModel -> addByIp ($ip);
			$this->AuthorizationModel -> assignPowers ($user);
			return;

	}

}
