<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SignupModel extends CI_Model {

	public function addByIp ($ip) {

		$saveId = md5(time ()).md5(time ());
		$this->db->query ("INSERT INTO `users` SET `ip` = ?, `saveId` = ?, `signupTime` = '".time ()."'", [$ip, $saveId]);
		setcookie('saveId', $saveId, time()+86400*24, '/', '.anime-base.ru');

		$user = $this->db->query("SELECT * FROM `users` WHERE `ip` = ?", [$ip])->row ();
		return $user;

	}

	public function addByVK ($data = []) {

		if (empty($data)) return;

		$this->db->query ("UPDATE `users` SET `vkId` = ?, `vkAccessToken` = ?, `firstName` = ?, `lastName` = ?, `avatar` = ? WHERE `ip` = ?", [$data['uid'], $data['access_token'], $data['first_name'], $data['last_name'], $data['photo_100'], $data['ip']]);

		return;

	}

}
