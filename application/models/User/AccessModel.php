<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AccessModel extends CI_Model {

	public function getById ($userId) {

		$query = $this->db->query ("SELECT * FROM `users` WHERE `id` = ?", [$userId]);
		return $query->row ();

	}

}