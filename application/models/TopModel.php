<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TopModel extends CI_Model {

	public function top ($limit = 5) {

		$query = $this->db->query ("SELECT * FROM `anime` ORDER BY `views` DESC LIMIT ".$limit);
		$result = $query->result ();

		return $result;
		
	}

	public function rand ($limit = 5) {

		$query = $this->db->query ("SELECT * FROM `anime` ORDER BY RAND() LIMIT ".$limit);
		$result = $query->result ();

		return $result;
		
	}

	public function getTop ($sorting = '', $sort='') {

		if (empty($sorting)) {
			$sqlSorting = '';
		}
		else if ($sorting == 'today') {
			$time = time () - 86400;
			$sqlSorting = "WHERE `animeView`.`time`>'".$time."'";
		}
		else if ($sorting == 'week') {
			$time = time () - 604800;
			$sqlSorting = "WHERE `animeView`.`time`>'".$time."'";
		}

		if (empty($sorting)) {
			$query = $this->db->query ("SELECT * FROM `anime` ORDER BY `views` DESC ".$sort);
		}
		else {
			$query = $this->db->query ("SELECT `anime`.*, count(`animeView`.`animeId`) AS `count` FROM `anime` LEFT JOIN `animeView` ON `anime`.`id`=`animeView`.`animeId` ".$sqlSorting." GROUP BY `anime`.`id` ORDER BY `count` DESC ".$sort);
		}

		$result = $query->result ();

		$data = [];

		$this->load->model ("AnimeModel");

		foreach ($result as $key => $value) {

			$anime = $this->AnimeModel->handler ($value);

			$data[$key] = $anime;

		}

		return $data;

	}

}