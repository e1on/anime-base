<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ViewModel extends CI_Model {

	public function add ($animeId, $ip) {

		$query = $this->db->query ("INSERT INTO `animeView` SET `animeId` = ?, `ip` = ?, `time` = '".time ()."'", [$animeId, $ip]);

		$this->db->query ("UPDATE `anime` SET `views` = `views`+1 WHERE `id` = ?", [$animeId]);
		return;

	}

	public function check ($animeId, $ip) {

		$query = $this->db->query ("SELECT * FROM `animeView` WHERE `animeId` = ? AND `ip` = ?", [$animeId, $ip]);
		$result = $query->num_rows ();

		return $result;

	}

	public function get ($animeId) {

		$query = $this->db->query ("SELECT * FROM `animeView` WHERE `animeId` = ?", [$animeId]);
		return $query->num_rows ();

	}

}